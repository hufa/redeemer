<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>{$html_title}</title>
	<!--meta name="viewport" content="width=700, shrink-to-fit=yes" id="viewport" /-->
	<meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=3, minimal-scale=0.4" />
	<link rel="icon" type="image/x-icon"          href="https://portal.funkfeuer.at/favicon.ico" />
	<link rel="shortcut icon" type="image/ico"    href="https://portal.funkfeuer.at/favicon.ico" />
	<link rel="apple-touch-icon" type="image/png" href="https://portal.funkfeuer.at/wien2.png" />
	<meta property="og:title" content="Redeemer Portal FunkFeuer-Wien" />
	<meta property="og:url" content="https://portal.funkfeuer.at/" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="https://portal.funkfeuer.at/wien2.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="200" />
	<meta property="og:image:height" content="128" />
	<link rel="stylesheet" type="text/css" media="screen" href="./css/main.css"/>
	<link rel="stylesheet" type="text/css" href="./css/icons.css" />
	<script language="JavaScript" src="./js/main.js"></script>
</head>

<body><div id="container">
	{if $credentials->isLoggedIn()}
		<div id="menu">
		<!--i class="fas fa-home"></i--> <a href="{$PHP_SELF}">Hauptseite</a> | 
		<!--i class="fas fa-user"></i--> <a href="{$PHP_SELF}?section=member&action=update">Benutzerdaten</a> | 
		<!--i class="fas fa-map-marker-alt"></i--> <a href="{$PHP_SELF}?section=nodes">Nodes</a> | 
		<!--i class="fas fa-phone-square"></i--> <a href="https://voip.funkfeuer.at/extensions/">VoIP</a> | 
		<!--<a href="{$PHP_SELF}?section=meetme&id_members={$credentials->getId()}">MeetMe</a> | -->
		<!--i class="fas fa-list"></i--> <a href="https://voip.funkfeuer.at/phonebook/">Phonebook</a> | 
		<!--i class="fas fa-door-open"></i--> <a href="{$PHP_SELF}?logoff=true">Logout</a>
		</div>
		<hr/>

		{if $credentials->isAdmin() or $credentials->isSuperuser()}
			{include file="admin.tpl"}
		{/if}

		{if $section eq "member"}
			{if $action eq "insert" and ($credentials->isAdmin() or $credentials->isSuperuser())}
				{if $do eq "true"}
					{$databaseUtils->insertMember()}
				{else}
					{$forms->member()}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updateMember()}
				{else}
					{$forms->member()}
				{/if}
			{elseif $action eq "search" and ($credentials->isAdmin() or $credentials->isSuperuser())}
				{$tables->members()}
			{elseif $action eq "delete" and $credentials->isAdmin()}
				{$databaseUtils->deleteMember()}
			{/if}
		{elseif $section eq "nodes"}
			{if $action eq "insert"}
				{if $do eq "true"}
					{$databaseUtils->insertNode()}
				{else}
					{$forms->node()}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updateNode()}
				{else}
					{$forms->node()}
				{/if}
			{elseif $action eq "delete"}
				{$databaseUtils->deleteNode()}
			{elseif $action eq "accept"}
				{$databaseUtils->acceptMemberChange()}
			{elseif $action eq "decline"}
				{$databaseUtils->declineMemberChange()}
			{elseif $action eq "resign"}
				{$databaseUtils->resignFromNode()}
			{elseif $action eq "proposeOwner"}
				{if $do eq "true"}
					{$databaseUtils->proposeMember('owner')}
				{else}
					{$forms->proposeOwner()}
				{/if}
			{elseif $action eq "proposeTechC"}
				{if $do eq "true"}
					{$databaseUtils->proposeMember('techC')}
				{else}
					{$forms->proposeTechC()}
				{/if}
			{else}
				{assign var=showErrors value="true"}
				{$tables->nodes($id_members)}
				{include file="../help/nodes.tpl"}
			{/if}
		{elseif $section eq "devices"}
			{if $action eq "insert"}
				{if $do eq "true"}
					{$databaseUtils->insertDevice()}
				{else}
					{$forms->device()}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updateDevice()}
				{else}
					{$forms->device()}
				{/if}
			{elseif $action eq "delete"}
				{$databaseUtils->deleteDevice()}
			{elseif $action eq "move"}
				{if $do eq "true"}
					{$databaseUtils->moveDevice()}
				{else}
					{$forms->moveDevice(4)}
				{/if}
			{elseif $action eq "move6"}
				{$forms->moveDevice(6)}
			{else}
				{assign var=showErrors value="true"}
				{$tables->devices($id_nodes)}
				{include file="../help/devices.tpl"}
			{/if}
		{elseif $section eq "ip6"}
			{if $action eq "insert"}
				{if $do eq "true"}
					{$databaseUtils->insertDevice6()}
				{else}
					{$forms->device6()}
					{include file="../help/device6form.tpl"}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updateDevice6()}
				{else}
					{$forms->device6()}
					{include file="../help/device6form.tpl"}
				{/if}
			{elseif $action eq "delete"}
				{$databaseUtils->deleteDevice6()}
			{else}
				{assign var=showErrors value="true"}
				{$tables->devices($id_nodes)}
				{include file="../help/devices.tpl"}
			{/if}
		{elseif $section eq "v642"}
			{if $action eq "insert"}
				{if $do eq "true"}
					{$databaseUtils->insertv642()}
				{else}
					{$forms->devicev642()}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updatev642()}
				{else}
					{$forms->devicev642()}
				{/if}
			{elseif $action eq "delete"}
				{$databaseUtils->deletev642()}
			{else}
				{assign var=showErrors value="true"}
				{$tables->devices($id_nodes)}
				{include file="../help/devices.tpl"}
			{/if}
		{elseif $section eq "voip"}
			{if $action eq "insert"}
				{if $do eq "true"}
					{$databaseUtils->insertVoip()}
				{else}
					{$forms->voip()}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updateVoip()}
				{else}
					{$forms->voip()}
				{/if}
			{elseif $action eq "delete"}
				{$databaseUtils->deleteVoip()}
			{else}
				{assign var=showErrors value="true"}
				{$tables->voip($id_members)}
				{include file="../help/voip.tpl"}
			{/if}
		{elseif $section eq "voip_sip"}
			{if $action eq "insert"}
				{if $do eq "true"}
					{$databaseUtils->insertVoipSip()}
				{else}
					{$forms->voip_sip()}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updateVoipSip()}
				{else}
					{$forms->voip_sip()}
				{/if}
			{elseif $action eq "delete"}
				{$databaseUtils->deleteVoipSip()}
			{else}
				{assign var=showErrors value="true"}
				{$tables->voip_sip($id_voip_extensions)}
				{include file="../help/voip_sip.tpl"}
			{/if}
		{elseif $section eq "meetme"}
			{if $action eq "insert"}
				{if $do eq "true"}
					{$databaseUtils->insertMeetme()}
				{else}
					{$forms->meetme()}
				{/if}
			{elseif $action eq "update"}
				{if $do eq "true"}
					{$databaseUtils->updateMeetme()}
				{else}
					{$forms->meetme()}
				{/if}
			{elseif $action eq "delete"}
				{$databaseUtils->deleteMeetme()}
			{else}
				{assign var=showErrors value="true"}
				{$tables->meetme($id_members)}
			{/if}
		{elseif $section eq "phonebook"}
			{$tables->phonebook($id_members)}
			{include file="../help/phonebook.tpl"}
		{else}
			{$databaseUtils->welcome()}
		{/if}

		{if $action eq "insert" or $action eq "update" or $action eq "delete" or $action eq "resign" or $action eq "accept" or $action eq "decline" or $action eq "move" or $action eq "search" or $action eq "proposeOwner" or $showErrors eq "true"}
			{include file="error.tpl"}
		{/if}

		{include file="debug.tpl"}
	{else}
		<center>
		{if $section eq "pwdreset" and $action eq "reset"}
			{if $databaseUtils->reset()}
				Password was reset. Check your mail... <hr>
				<br><a href="{$PHP_SELF}">back to login</a>
			{else}
				<hr> {$forms->pwdreset()}
				<br><a href="{$PHP_SELF}">back to login</a>
			{/if}
		{elseif $section eq "pwdreset"}
			{$forms->pwdreset()}
			<br><a href="{$PHP_SELF}">back to login</a>
		{else}
			{$forms->login()}
			<br><a href="{$PHP_SELF}?section=pwdreset">forgot your password?</a>
		{/if}
		</center>
	{/if}
	<div class=footer>&copy;2020 <a href="http://sourceforge.net/p/redeemer">redeemer.frontend</a> + <a href="https://gitlab.com/funkfeuer/redeemer">0xff mods</a>{$add_on}</div>
</div>
</body>
</html>

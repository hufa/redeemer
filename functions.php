<?php

function generatePassword ($length = 8) {
// taken from http://www.laughing-buddha.net/jon/php/password/

	// start with a blank password
	$password = "";

	// define possible characters
	$possible = "0123456789bcdfghjkmnpqrstvwxyzABCDEFGHJKLMNPQRSTUVWXYZ@:-()!$%";

	// set up a counter
	$i = 0;

	// add random characters to $password until $length is reached
	while ($i < $length) {

		// pick a random character from the possible ones
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);

		// we don't want this character if it's already in the password
		if (!strstr($password, $char)) {
			$password .= $char;
			$i++;
		}

	}

	// done!
	return $password;
}

function mailNewPassword($nickname, $newPassword, $email) {
	$mail_headers  = "From: ".MEMBER_SUBSCRIBE_MAIL_FROM."\r\n";
	$mail_headers  = "To: ".$email."\r\n";
	$mail_headers .= "MIME-Version: 1.0\r\n";
	$mail_headers .= "Content-Type: text/plain; charset=utf-8\r\n";
	$mail_headers .= "Content-Transfer-Encoding: quoted-printable\r\n";

	$mail_content  = "This is your new password:\n$newPassword\r\n\r\n";
	$mail_content .= "Please, change it as soon as you login:\r\n";
	$mail_content .= "https://portal.funkfeuer.at/wien/\r\n";
	$mail_content .= "\r\n";
	$mail_content .= "BR,\r\n";
	$mail_content .= "The FunkFeuer Crew\r\n";

	mail($email, MEMBER_SUBSCRIBE_MAIL_SUBJECT, $mail_content, $mail_headers, "-f".MEMBER_SUBSCRIBE_MAIL_FROM);
}

function testRange($int,$min,$max) {
	return (($min<=$int) && ($int<=$max));
}

function type_of_ip($ip) {
	if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
		return 'IPv6';
	} elseif (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
		return 'IPv4';
	} else {
		return false;
	}
}

function is_valid_domain_name($domain_name) {
	return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) //valid chars check
		&& preg_match("/^.{1,253}$/", $domain_name) //overall length check
		&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name)   ); //length of each label
}


?>

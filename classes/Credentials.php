<?php

class Credentials {

	var $id = null;
	var $nickname = null;
	var $firstname = null;
	var $loggedIn = false;
	var $admin = false;
	var $superuser = false;

	function load($nickname, $password) {
		global $databaseUtils;
		$call=$databaseUtils->getFirstRow("SELECT member_login('".strtolower($nickname)."',MD5('".$password."')) as result");
		if ($call['result']>0) {
			$row = $databaseUtils->getFirstRow('SELECT id, nickname, firstname '.
									'FROM members '.
									'WHERE id= '.$call['result']);
			if (!empty($row['id'])) {
				$this->id = $row['id'];
				$this->nickname = $row['nickname'];
				$this->firstname= $row['firstname'];
				$this->loggedIn = true;
				setcookie('CSRFtoken','', time()-3600);

				$adminRow = $databaseUtils->getFirstRow('SELECT min(id_roles) AS id '.
												'FROM roles, members_roles '.
												'WHERE roles.id=members_roles.id_roles '.
												'AND roles.name in (\''.ADMIN_ROLE_NAME.'\',\''.SUPERUSER_ROLE_NAME.'\') '.
												'AND members_roles.id_members='.$row['id']);
				if ($adminRow['id'] == 1) {
					$this->admin = true;
				} elseif ($adminRow['id'] == 2) {
					$this->superuser = true;
				}
				// exit with external redirect
				storeCredentials();
				header("HTTP/1.1 301 Moved Permanently");
				header("X-Login-OK: User=".$this->nickname.", Id=".$this->id);
				header("Location: ".$_SERVER['REQUEST_URI']);
				exit;
			}
		}
		/*login failed*/
		header("X-Login-Failed: User/Password error");
	}

	function getId() {
		return $this->id;
	}

	function getNickname() {
		return $this->nickname;
	}

	function getFirstname() {
		return $this->firstname;
	}

	function isLoggedIn() {
		return $this->loggedIn;
	}

	function isAdmin() {
		return $this->admin;
	}
	function isSuperuser() {
		return $this->superuser;
	}

	function toString() {
		return 'id='.$this->id.','.
			'nickname='.$this->nickname.','.
			'loggedIn='.ConvertUtils::escape($this->loggedIn, 'boolean').','.
			'admin='.ConvertUtils::escape($this->admin, 'boolean');
	}

}

?>

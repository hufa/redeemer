<?php

class Tables {
	function nodes($id) {
		global $databaseUtils, $credentials;
		if (!isset($id) || $id==null) $id=$credentials->getId();

		if (checkRights($id, 'members')) {
			$table = new HTML_Table();
			$table->setCaption('Nodes [<a href="'.$_SERVER['REQUEST_URI'].'&action=insert">hinzuf&uuml;gen</a>]');
			$table->setColCount(6);
			$table->setHeaderContents(0, 0, 'Name');
			$table->setColAttributes(0,'style="width:'.COLWIDTH_NAME.'px; height:35px;"', true);
			$table->setHeaderContents(0, 1, 'Tech-Contact');
			$table->setColAttributes(1,'style="width:'.COLWIDTH_TECHC.'px;"', true);
			$table->setHeaderContents(0, 2, 'Position');
			$table->setColAttributes(2,'style="width:'.COLWIDTH_GPS.'px;"', true);
			$table->setHeaderContents(0, 3, 'Map');
			$table->setColAttributes(3,'style="width:'.COLWIDTH_MAP.'px;"', true);
			$table->setHeaderContents(0, 4, 'Devices');
			$table->setColAttributes(4,'style="width:'.COLWIDTH_DEVICES.'px;"', true);
			$table->setHeaderContents(0, 5, 'Aktionen');
			$table->setHeaderContents(0, 6, 'Node-ID');
			$result = $databaseUtils->getRows('SELECT n.*, (select count(*) from devices d where d.id_nodes=n.id) anz_dev4 '.
				' ,(select count(*) from ip6 i where i.id_nodes=n.id) anz_dev6 '.
				' ,(select count(*) from ip6 b, devices e where e.id_nodes=n.id and b.id_devices=e.id) anz_dev6d '.
				' ,t.id_tech_c id_proposed, o.id_members owner_proposed '.
				" ,case when (n.last_seen >= now()-interval '4' hour and to_char(n.last_seen,'hh24miss')!='000000') then 1 else 0 end as node_online ".
				'FROM nodes n LEFT OUTER JOIN nodes t on (n.id=-t.id and t.id<0 and t.id_members =n.id_members) '.
				'LEFT OUTER JOIN nodes o on (n.id=-o.id and o.id<0 and o.id_members!=n.id_members) '.
				'WHERE n.id>=0 AND n.id_members='.ConvertUtils::escape($id, 'integer').' ORDER BY lower(n.name)');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$techc=""; $techc_proposed="";
				$size='';
				//$size=' fa-2x';
				if ($row['id_tech_c']!=$row['id_members']) {
					$size='';
					$tech_row = $databaseUtils->getFirstRow('SELECT * from members where id='.$row['id_tech_c']);
					$techc='<a href="mailto:'.$tech_row['email'].'">'.$tech_row['firstname'].'&nbsp;'.$tech_row['lastname'].'</a>&nbsp;<a  href="'.$_SERVER['REQUEST_URI'].'&action=resign&id='.$row['id'].'" title="Tech-C entfernen" onclick="return askResign()"><i class="fas fa-times"></i></a>';
				}
				if ($row['id_proposed']>0) {
					$size='';
					$tech_row = $databaseUtils->getFirstRow('SELECT * from members where id='.$row['id_proposed']);
					$techc_proposed='<i class="fas fa-hourglass-end" title="Einladung versendet und noch nicht angenommen"></i><a href="mailto:'.$tech_row['email'].'">'.substr($tech_row['firstname'],0,1).'&nbsp;'.$tech_row['lastname'].'</a>&nbsp;<a  href="'.$_SERVER['REQUEST_URI'].'&action=resign&id='.$row['id'].'&p='.$tech_row['id'].'" title="Einladung zu Tech-C entfernen" onclick="return askResign()"><i class="fas fa-times"></i></a><br>';
				}

				if ($row['owner_proposed']>0) {
					$move_row = $databaseUtils->getFirstRow('SELECT * from members where id='.$row['owner_proposed']);
					$move_proposed='&nbsp;<i class="fas fa-hourglass-end fa-2x" title="Weitergabe an '.substr($move_row['firstname'],0,1).'.'.$move_row['lastname'].' wartet auf Bestätigung"></i><a  href="'.$_SERVER['REQUEST_URI'].'&action=decline&id='.$row['id'].'&p='.$move_row['id'].'" title="Weitergabe abbrechen" onclick="return askDecline()"><i class="fas fa-times fa-2x"></i></a>';
				} else {
					$move_proposed="";
				}
				if (is_numeric($row['gps_lat_deg']) && ($row['gps_lat_deg']!=0) && is_numeric($row['gps_lat_min']) && is_numeric($row['gps_lat_sec']))
					$latitude= number_format(  $row['gps_lat_deg'] + $row['gps_lat_min']/60 + $row['gps_lat_sec']/3600 ,4);
				else $latitude='-';
				if (is_numeric($row['gps_lon_deg']) && ($row['gps_lon_deg']!=0) && is_numeric($row['gps_lon_min']) && is_numeric($row['gps_lon_sec']))
					$longitude= number_format(  $row['gps_lon_deg'] + $row['gps_lon_min']/60 + $row['gps_lon_sec']/3600 ,4);
				else $longitude='-';
				$table->addRow(array('<b><a href="'.$_SERVER['REQUEST_URI'].'&action=update&id='.$row['id'].'"><div style="line-height: 25px; vertical-align: middle;">'.$row[name].'</div></a></b>',
					'<center>'.$techc_proposed.$techc.'&nbsp;<a href="'.$_SERVER['REQUEST_URI'].'&action=proposeTechC&id='.$row['id'].'" title="Ändern, neuen Tech-C ansuchen"><i class="far fa-edit '.$size.'"></i></a></center>',
					'<center>'.$latitude.'<br>'.$longitude.'</center>',
					ConvertUtils::buildCheckbox($row['map']),
					'<center><a href="'  .$_SERVER['PHP_SELF'].  '?section=devices&id_nodes='  .$row['id']. '" title="'.
					  ($row['anz_dev4']+$row['anz_dev6']+$row['anz_dev6d']).(($row['anz_dev6']+$row['anz_dev6d']>0) ? ": \n".$row['anz_dev4'].'(v4) + '.($row['anz_dev6']+$row['anz_dev6d']).'(v6)' : '').'"><i class="fas fa-network-wired fa-2x" '.
					  ($row['node_online'] ? 'style="color:green;"' : '').'></i></a></center>',
					(($row['anz_dev4']+$row['anz_dev6']+$row['anz_dev6d']  == 0) ?
					  '<center><a href="'.$_SERVER['REQUEST_URI'].'&action=delete&id='.$row['id'].'" onclick="return askDelete()" title="Löschen"><i class="far fa-trash-alt fa-2x" ></i></a>' :
					  '<center><i class="far fa-trash-alt fa-2x" style="color: gray;" title="Devices vorhanden: Löschen nicht möglich"></i>').
					($move_proposed ? $move_proposed.'</center>' : '&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$_SERVER['REQUEST_URI'].'&action=proposeOwner&id='.$row['id'].'" title="Weitergeben"><i class="fas fa-exchange-alt fa-2x"></i></a></center>'),
					'<center>'.$row['id'].'<br>0x'.str_pad(strtoupper(dechex($row['id'])),4,'0',STR_PAD_LEFT).'</center>'
				));
			}
			echo $table->toHtml();

			$techc_count=0;
			$table = new HTML_Table();
			$table->setCaption('Du bist Tech-Contact von diesen Nodes:');
			$table->setColCount(6);
			$table->setHeaderContents(0, 0, 'Name');
			$table->setColAttributes(0,'style="width:'.COLWIDTH_NAME.'px; height:35px;"', true);
			$table->setHeaderContents(0, 1, 'Besitzer');
			$table->setColAttributes(1,'style="width:'.COLWIDTH_TECHC.'px;"', true);
			$table->setHeaderContents(0, 2, 'Position');
			$table->setColAttributes(2,'style="width:'.COLWIDTH_GPS.'px;"', true);
			$table->setHeaderContents(0, 3, 'Map');
			$table->setColAttributes(3,'style="width:'.COLWIDTH_MAP.'px;"', true);
			$table->setHeaderContents(0, 4, 'Devices');
			$table->setColAttributes(4,'style="width:'.COLWIDTH_DEVICES.'px;"', true);
			$table->setHeaderContents(0, 5, 'Verzichten');
			$table->setHeaderContents(0, 6, 'Node-ID');
			$result = $databaseUtils->getRows('SELECT n.*, (select count(*) from devices d where d.id_nodes=n.id) anz_dev4 '.
				' ,(select count(*) from ip6 i where i.id_nodes=n.id) anz_dev6 '.
				' ,(select count(*) from ip6 b, devices e where e.id_nodes=n.id and b.id_devices=e.id) anz_dev6d '.
				' ,t.id_tech_c id_proposed, o.id_members owner_proposed '.
				" ,case when (n.last_seen >= now()-interval '4' hour and to_char(n.last_seen,'hh24miss')!='000000') then 1 else 0 end as node_online ".
				'FROM nodes n LEFT OUTER JOIN nodes t on (n.id=-t.id and t.id<0 and t.id_members =n.id_members) '.
				'LEFT OUTER JOIN nodes o on (n.id=-o.id and o.id<0 and o.id_members!=n.id_members) '.
				'WHERE n.id >= 0 AND n.id_tech_c='.ConvertUtils::escape($id, 'integer').' AND n.id_members!='.ConvertUtils::escape($id, 'integer').' ORDER BY lower(n.name)');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$techc_count++;
				//get memeber name/email
				$member_result = $databaseUtils->getRows('SELECT * from members where id='.$row['id_members']);
				$member_row = $member_result->fetchRow(DB_FETCHMODE_ASSOC);
				if (is_numeric($row['gps_lat_deg']) && ($row['gps_lat_deg']!=0) && is_numeric($row['gps_lat_min']) && is_numeric($row['gps_lat_sec']))
					$latitude= number_format(  $row['gps_lat_deg'] + $row['gps_lat_min']/60 + $row['gps_lat_sec']/3600 ,4);
				else $latitude='-';
				if (is_numeric($row['gps_lon_deg']) && ($row['gps_lon_deg']!=0) && is_numeric($row['gps_lon_min']) && is_numeric($row['gps_lon_sec']))
					$longitude= number_format(  $row['gps_lon_deg'] + $row['gps_lon_min']/60 + $row['gps_lon_sec']/3600 ,4);
				else $longitude='-';
				$table->addRow(array('<b><a href="'.$_SERVER['REQUEST_URI'].'&action=update&id='.$row['id'].'"><div style="line-height: 25px; vertical-align: middle;">'.$row[name].'</div></a></b>',
					'<center><a href="mailto:'.$member_row['email'].'">'.$member_row['firstname'].' '.$member_row['lastname'].'</a></center>',
					'<center>'.$latitude.'<br>'.$longitude.'</center>',
					ConvertUtils::buildCheckbox($row['map']),
					'<center><a href="'  .$_SERVER['PHP_SELF'].  '?section=devices&id_nodes='  .$row['id']. '" title="'.
					  ($row['anz_dev4']+$row['anz_dev6']+$row['anz_dev6d']).(($row['anz_dev6']+$row['anz_dev6d']>0) ? ": \n".$row['anz_dev4'].'(v4) + '.($row['anz_dev6']+$row['anz_dev6d']).'(v6)' : '').'"><i class="fas fa-network-wired fa-2x" '.
					  ($row['node_online'] ? 'style="color:green;"' : '').'></i></a></center>',
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=resign&id='.$row['id'].'" onclick="return askResign()" title="Verzichten"><i class="fas fa-times fa-2x"></i></a></center>',
					'<center>'.$row['id'].'<br>0x'.str_pad(strtoupper(dechex($row['id'])),4,'0',STR_PAD_LEFT).'</center>'
				));
			}
			if ($techc_count>0) {
				echo "<hr>";
				echo $table->toHtml();
			}

			$propose_count=0;
			$table = new HTML_Table();
			$table->setCaption('Folgende User haben dich fuer Ihren Knoten als Tech-Contact vorgeschlagen:');
			$table->setColCount(6);
			$table->setHeaderContents(0, 0, 'Name');
			$table->setColAttributes(0,'style="width:'.COLWIDTH_NAME.'px; height:35px;"', true);
			$table->setHeaderContents(0, 1, 'Besitzer');
			$table->setColAttributes(1,'style="width:'.COLWIDTH_TECHC.'px;"', true);
			$table->setHeaderContents(0, 2, 'Position');
			$table->setColAttributes(2,'style="width:'.COLWIDTH_GPS.'px;"', true);
			$table->setHeaderContents(0, 3, 'Map');
			$table->setColAttributes(3,'style="width:'.COLWIDTH_MAP.'px;"', true);
			$table->setHeaderContents(0, 4, 'Akzeptieren');
			$table->setHeaderContents(0, 5, 'Ablehnen');
			$result = $databaseUtils->getRows('SELECT * from nodes WHERE id in (SELECT -id FROM nodes WHERE id < 0 AND id_members<>'.ConvertUtils::escape($id, 'integer').' AND id_tech_c='.ConvertUtils::escape($id, 'integer').') AND id_members<>'.ConvertUtils::escape($id, 'integer').';');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$propose_count++;
				//get member name/email
				$member_result = $databaseUtils->getRows('SELECT * from members where id='.$row['id_members']);
				$member_row = $member_result->fetchRow(DB_FETCHMODE_ASSOC);
				if (is_numeric($row['gps_lat_deg']) && ($row['gps_lat_deg']!=0) && is_numeric($row['gps_lat_min']) && is_numeric($row['gps_lat_sec']))
					$latitude= number_format(  $row['gps_lat_deg'] + $row['gps_lat_min']/60 + $row['gps_lat_sec']/3600 ,4);
				else $latitude='-';
				if (is_numeric($row['gps_lon_deg']) && ($row['gps_lon_deg']!=0) && is_numeric($row['gps_lon_min']) && is_numeric($row['gps_lon_sec']))
					$longitude= number_format(  $row['gps_lon_deg'] + $row['gps_lon_min']/60 + $row['gps_lon_sec']/3600 ,4);
				else $longitude='-';
				$table->addRow(array('<b>'.$row[name].'</b>',
					'<center><a href="mailto:'.$member_row['email'].'">'.$member_row['firstname'].' '.$member_row['lastname'].'</a></center>',
					'<center>'.$latitude.'<br>'.$longitude.'</center>',
					ConvertUtils::buildCheckbox($row['map']),
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=accept&id='.$row['id'].'" title="Akzeptieren"><i class="fas fa-check fa-2x"></i></a></center>',
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=decline&id='.$row['id'].'" title="Ablehnen"><i class="fas fa-times fa-2x"></i></a></center>'
				));
			}
			if ($propose_count>0) {
				echo "<hr>";
				echo $table->toHtml();
			}

			$transfer_count=0;
			$table = new HTML_Table();
			$table->setCaption('Folgende User haben dich fuer ihre Knoten als neuer <b>Besitzer</b> vorgeschlagen:');
			$table->setColCount(6);
			$table->setHeaderContents(0, 0, 'Name');
			$table->setColAttributes(0,'style="width:'.COLWIDTH_NAME.'px; height:35px;"', true);
			$table->setHeaderContents(0, 1, 'Besitzer');
			$table->setColAttributes(1,'style="width:'.COLWIDTH_TECHC.'px;"', true);
			$table->setHeaderContents(0, 2, 'Position');
			$table->setColAttributes(2,'style="width:'.COLWIDTH_GPS.'px;"', true);
			$table->setHeaderContents(0, 3, 'Map');
			$table->setColAttributes(3,'style="width:'.COLWIDTH_MAP.'px;"', true);
			$table->setHeaderContents(0, 4, 'Akzeptieren');
			$table->setHeaderContents(0, 5, 'Ablehnen');
			$result = $databaseUtils->getRows('SELECT * from nodes WHERE id in (SELECT -id FROM nodes WHERE id < 0 AND id_members='.ConvertUtils::escape($id, 'integer').') AND id_members<>'.$databaseUtils->my_stuff['members'][0].';');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$transfer_count++;
				//get member name/email
				$member_result = $databaseUtils->getRows('SELECT * from members where id='.$row['id_members']);
				$member_row = $member_result->fetchRow(DB_FETCHMODE_ASSOC);
				if (is_numeric($row['gps_lat_deg']) && ($row['gps_lat_deg']!=0) && is_numeric($row['gps_lat_min']) && is_numeric($row['gps_lat_sec']))
					$latitude= number_format(  $row['gps_lat_deg'] + $row['gps_lat_min']/60 + $row['gps_lat_sec']/3600 ,4);
				else $latitude='-';
				if (is_numeric($row['gps_lon_deg']) && ($row['gps_lon_deg']!=0) && is_numeric($row['gps_lon_min']) && is_numeric($row['gps_lon_sec']))
					$longitude= number_format(  $row['gps_lon_deg'] + $row['gps_lon_min']/60 + $row['gps_lon_sec']/3600 ,4);
				else $longitude='-';
				$table->addRow(array('<b>'.$row[name].'</b>',
					'<center><a href="mailto:'.$member_row['email'].'">'.$member_row['firstname'].' '.$member_row['lastname'].'</a></center>',
					'<center>'.$latitude.'<br>'.$longitude.'</center>',
					ConvertUtils::buildCheckbox($row['map']),
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=accept&id='.$row['id'].'" title="Akzeptieren"><i class="fas fa-check fa-2x"></i></a></center>',
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=decline&id='.$row['id'].'" title="Ablehnen"><i class="fas fa-times fa-2x"></i></a></center>'
				));
			}
			if ($transfer_count>0) {
				echo "<hr>";
				echo $table->toHtml();
			}
		}
	}

	function devices($id) {
		global $databaseUtils, $node_name;
		$moveable=0;

		if (checkRights($id, 'nodes')) {
			if (count($databaseUtils->my_stuff['nodes']) > 1) $moveable=1;
			$uri2=str_replace('section=devices','section=v642',$_SERVER['REQUEST_URI']);
			$table = new HTML_Table();
			$table->setCaption('Node: <b>'.$node_name.'</b><br><br>Devices [<a href="'.$_SERVER['REQUEST_URI'].'&action=insert">hinzuf&uuml;gen</a>] - '.
				'[<a href=\'javascript:toggleTable(4)\' id="toggleButton4">Show Help</a>]');
			$table->setColCount(9);
			$table->setHeaderContents(0, 0, 'Name');
			$table->setHeaderContents(0, 1, 'Antenne');
			$table->setColAttributes(1,'class="collapsable_1"', true);
			$table->setHeaderContents(0, 2, 'Hardware');
			$table->setColAttributes(2,'class="collapsable_2"', true);
			$table->setHeaderContents(0, 3, 'SSID');
			$table->setColAttributes(3,'class="collapsable_3"', true);
			$table->setHeaderContents(0, 4, 'MAC');
			$table->setColAttributes(4,'class="collapsable_4"', true);
			$table->setColAttributes(4,'style="width:'.COLWIDTH_MAC.'px;"', true);
			$table->setHeaderContents(0, 5, 'SmokePing');
			$table->setHeaderContents(0, 6, 'Zuletzt&nbsp;online');
			$table->setColAttributes(6,'style="width:'.COLWIDTH_LASTSEEN.'px; height:35px;"', true);
			$table->setHeaderContents(0, 7, 'IP');
			$table->setHeaderContents(0, 8, 'Aktionen');
			$table->setCellAttributes(0, 8,"colspan=2");
			$result = $databaseUtils->getRows("SELECT devices.*, ips.ip, ips.usage, case when (devices.last_seen >= now()-interval '4' hour and to_char(devices.last_seen,'hh24miss')!='000000') then 1 else 0 end as dev_online ".
				'FROM devices, ips WHERE devices.id=ips.id_devices AND devices.id_nodes='.ConvertUtils::escape($id, 'integer').'ORDER BY usage, lower(name)');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				if ($row['nodemaster'] == 't') $nm='<i class="fas fa-download" style="float:right; padding-top:5px;" title="NodeMasterIP"></i>'; else $nm='';
				if ($row['usage'] == 'v642') {
					$table->addRow(array('<a href="'.$uri2.'&action=update&id='.$row['id'].'"><div style="line-height: 25px; vertical-align: middle;">'.$row['name'].$nm.'</div></a>',
							 $row['antenna'],
							 $row['hardware'],
							 $row['ssid'],
							 $row['mac'],
							/*ConvertUtils::buildCheckbox($row['smokeping'])*/'<center>'.
							//show link to smokeping page if enabled and corresponding smokeping files do exist
							'<center>'.($row['smokeping']=='t'?
								'<a target=smokeping href="/olsr2/smokeping/?target='.$node_name.'.'.$row['name'].'__v4">ja</a>'
								:'nein').'</center>',
							 '<span '.($row['dev_online'] ? 'style="color:green;"' : '').'>'.format_date($row['last_seen']).'</span>',//'<iframe height=16 width=100 style="border:none;" src="lastseen.php?host='.$row['ip'].'" ></iframe>',
							 '<a href="http://'.$row['ip'].'/">'.$row['ip'].'</a>',
							 '<center><a href="'.$uri2.'&action=delete&id='.$row['id'].'" onclick="return askDelete()"><i class="far fa-trash-alt fa-2x" ></i></a></center>',
							 ''
						));
					$table->setRowAttributes($table->getRowCount() -1,"style='background:#b4daff;'");
					$table->setCellAttributes($table->getRowCount() -1, 1,"style='background:#b4daff;'".' class="collapsable_1"', true);
					$table->setCellAttributes($table->getRowCount() -1, 2,"style='background:#b4daff;'".' class="collapsable_2"', true);
					$table->setCellAttributes($table->getRowCount() -1, 3,"style='background:#b4daff;'".' class="collapsable_3"', true);
					$table->setCellAttributes($table->getRowCount() -1, 4,"style='background:#b4daff;'".' class="collapsable_4"', true);
				} else {
					$table->addRow(array('<a href="'.$_SERVER['REQUEST_URI'].'&action=update&id='.$row['id'].'"><div style="line-height: 25px; vertical-align: middle;">'.$row['name'].$nm.'</div></a>',
						$row['antenna'],
						$row['hardware'],
						$row['ssid'],
						$row['mac'],
						/*ConvertUtils::buildCheckbox($row['smokeping'])*/'<center>'.
						//show link to smokeping page if enabled and corresponding smokeping files do exist
						'<center>'.($row['smokeping']=='t'?
							'<a target=smokeping href="smokeping/?target='.$node_name.'.'.$row['name'].'">ja</a>'
							:'nein').'</center>',
						'<span '.($row['dev_online'] ? 'style="color:green;"' : '').'>'.format_date($row['last_seen']).'</span>',//'<iframe height=16 width=100 style="border:none;" src="lastseen.php?host='.$row['ip'].'" ></iframe>',
						'<a href="http://'.$row['ip'].'/">'.$row['ip'].'</a>',
						'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=delete&id='.$row['id'].'" onclick="return askDelete()" title="Löschen"><i class="far fa-trash-alt fa-2x" ></i></a></center>',
						($moveable?'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=move&id='.$row['id'].'" title="Verschieben"><i class="fas fa-sign-out-alt fa-2x"></i></a></center>':'')
					));
					$table->setColAttributes(1,'class="collapsable_1"', true);
					$table->setColAttributes(2,'class="collapsable_2"', true);
					$table->setColAttributes(3,'class="collapsable_3"', true);
					$table->setColAttributes(4,'class="collapsable_4"', true);
				}
			}
			echo $table->toHtml();
			echo '<br>';

			//add ipv6 table
			$v642count = $databaseUtils->getFirstRow('SELECT count(*) anzahl FROM devices d, ips i WHERE d.id=i.id_devices AND i.usage=\'v642\' AND d.id_nodes='.ConvertUtils::escape($id, 'integer'));
			$uri=str_replace('section=devices','section=ip6',$_SERVER['REQUEST_URI']);
			$table = new HTML_Table();
			$table->setCaption('<br><br>IPv6-Adressen [<a href="'.$uri.'&action=insert">hinzuf&uuml;gen</a>]'.(($v642count['anzahl']==0) ? ' - [<a href="'.$uri2.'&action=insert">v642 anlegen</a>] - ' : ' - ').
				'[<a href=\'javascript:toggleTable(6)\' id="toggleButton6">Show Help</a>]');
			$table->setColCount(6);
			$table->setHeaderContents(0, 0, 'Name');
			$table->setHeaderContents(0, 1, 'Zuordnung');
			$table->setColAttributes(1,'class="collapsable_3"', true);
			//$table->setHeaderContents(0, 1, 'Antenne');
			//$table->setHeaderContents(0, 2, 'Hardware');
			//$table->setHeaderContents(0, 3, 'SSID');
			$table->setHeaderContents(0, 2, 'MAC');
			$table->setColAttributes(2,'class="collapsable_2"', true);
			$table->setColAttributes(2,'style="width:'.COLWIDTH_MAC.'px;"', true);
			$table->setHeaderContents(0, 3, 'SmokePing');
			$table->setColAttributes(3,'class="collapsable_4"', true);
			$table->setHeaderContents(0, 4, 'DNS');
			$table->setColAttributes(4,'style="width:'.COLWIDTH_AAAA.'px;" class="collapsable_5"', true);
			$table->setHeaderContents(0, 5, 'Zuletzt&nbsp;online');
			$table->setColAttributes(5,'style="width:'.COLWIDTH_LASTSEEN.'px;"', true);
			$table->setHeaderContents(0, 6, 'IPv6');
			$table->setColAttributes(6,'style="width:'.COLWIDTH_IPV6.'px; height:35px;"', true);
			$table->setHeaderContents(0, 7, 'Aktionen');
			$table->setCellAttributes(0, 7,"colspan=2");
			$result = $databaseUtils->getRows('select \'n\' what, i.id, i.name, i.ip6, i.dns_forward, i.dns_reverse, i.antenna, i.hardware, i.ssid, i.mac, i.smokeping, i.nodemaster,'.
				' i.last_seen, null dev, \'main\' usage, lower(i.name) sort, '."case when (i.last_seen >= now()-interval '4' hour and to_char(i.last_seen,'hh24miss')!='000000') then 1 else 0 end as dev_online".' from ip6 i'.
				' where i.id_nodes='.ConvertUtils::escape($id, 'integer').' UNION ALL'.
				' select  \'d\' what, case when v.usage=\'v642\' then d.id else i.id end as id, coalesce(i.name,d.name), i.ip6, i.dns_forward, i.dns_reverse, i.antenna, i.hardware, i.ssid, i.mac, i.smokeping, i.nodemaster, '.
				' i.last_seen, d.name dev , v.usage, lower(coalesce(i.name,d.name)) sort, '."case when (i.last_seen >= now()-interval '4' hour and to_char(i.last_seen,'hh24miss')!='000000') then 1 else 0 end as dev_online".' from ip6 i, devices d, ips v'.
				' where i.id_devices=d.id and d.id_nodes='.ConvertUtils::escape($id, 'integer').' and v.id_devices=d.id ORDER BY usage, sort'
			);
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				if ($row['nodemaster'] == 't') $nm='<i class="fas fa-download" style="float:right; padding-top:5px;" title="NodeMasterIP"></i>'; else $nm='';
				if ($row['usage'] == 'v642') {
					$table->addRow(array(
						'<a href="'.$uri2.'&action=update&id='.$row['id'].'"><div style="line-height: 25px; vertical-align: middle;">'.$row['name'].$nm.'</div></a>',
						($row['what']=='n' ? '('.$node_name.')' : $row['dev']),
						//$row['antenna'],
						//$row['hardware'],
						//$row['ssid'],
						$row['mac'],
						'<center>'.($row['smokeping']=='t'?
							'<a target=smokeping href="/olsr2/smokeping/?target='.$node_name.'.'.$row['name'].'">ja</a>'
							:'nein').'</center>',
						'<center>'.ConvertUtils::buildCheckbox($row['dns_forward'],false).ConvertUtils::buildCheckbox($row['dns_reverse'],false).'</center>',
						'<span '.($row['dev_online'] ? 'style="color:green;"' : '').'>'.format_date($row['last_seen']).'</span>',
						'<a href="http://['.$row['ip6'].']/">'.$row['ip6'].'</a>',
						'<center>-</center>',
						'<center>-</center>'
					));
					$table->setRowAttributes($table->getRowCount() -1,"style='background:#b4daff;'" );
					$table->setCellAttributes($table->getRowCount() -1, 1,"style='background:#b4daff;'".' class="collapsable_3"', true);
					$table->setCellAttributes($table->getRowCount() -1, 2,"style='background:#b4daff;'".' class="collapsable_2"', true);
					$table->setCellAttributes($table->getRowCount() -1, 3,"style='background:#b4daff;'".' class="collapsable_4"', true);
					$table->setCellAttributes($table->getRowCount() -1, 4,"style='background:#b4daff;'".' class="collapsable_5"', true);
				} else {
					if ( ($moveable) && (eregi("^2a02:61:0:.*$",$row['ip6'])) ) { $movethis=true; } else { $movethis=false; }
					$table->addRow(array(
						'<a href="'.$uri.'&action=update&id='.$row['id'].'"><div style="line-height: 25px; vertical-align: middle;">'.$row['name'].$nm.'</div></a>',
						($row['what']=='n' ? '('.$node_name.')' : $row['dev']),
						//$row['antenna'],
						//$row['hardware'],
						//$row['ssid'],
						$row['mac'],
						'<center>'.($row['smokeping']=='t'?
							'<a target=smokeping href="/olsr2/smokeping/?target='.$node_name.'.'.$row['name'].'">ja</a>'
							:'nein').'</center>',
						'<center>'.ConvertUtils::buildCheckbox($row['dns_forward'],false).ConvertUtils::buildCheckbox($row['dns_reverse'],false).'</center>',
						'<span '.($row['dev_online'] ? 'style="color:green;"' : '').'>'.format_date($row['last_seen']).'</span>',
						'<a href="http://['.$row['ip6'].']/">'.$row['ip6'].'</a>',
						'<center><a href="'.$uri.'&action=delete&id='.$row['id'].'" onclick="return askDelete()" title="Löschen"><i class="far fa-trash-alt fa-2x"></i></a></center>',
						($movethis?'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=move6&id='.$row['id'].'" title="Verschieben"><i class="fas fa-sign-out-alt fa-2x"></i></a></center>':'<center>-</center>')
					));
					$table->setColAttributes(1,'class="collapsable_3"', true);
					$table->setColAttributes(2,'class="collapsable_2"', true);
					$table->setColAttributes(3,'class="collapsable_4"', true);
					$table->setColAttributes(4,'class="collapsable_5"', true);
				}
			}
			echo $table->toHtml();
		}
	}

		function members() {
			global $databaseUtils, $credentials;

			if (($credentials->isAdmin()) || ($credentials->isSuperuser())) {
				if ((strlen(str_replace('%','',$_SESSION['search'])) == 0) && (strlen($_SESSION['ingroups'])==0) && (strlen($_SESSION['offgroups'])==0)) {
					?>
<form method="post" action="<?php echo $PHP_SELF; ?>?section=member&action=search">
Suchwort: <input type=text name=search placeholder="Suchstring" value="<?php echo $_SESSION['search']; ?>"><br>
InGruppen: <input type=text name=ingroups placeholder="3,4" value="<?php echo $_SESSION['ingroups']; ?>"><br>
NotGruppen: <input type=text name=offgroups placeholder="7" value="<?php echo $_SESSION['offgroups']; ?>"><br>
Limit Result: <input type=checkbox name=limitresult <?php echo $_SESSION['limitresult']; ?> ><br>
<input type=submit name=submit value="search"></form>
<?php
				} elseif ((strlen(str_replace('%','',$_SESSION['search'])) < 3 ) && (!is_numeric($_SESSION['search'])) && (strlen($_SESSION['ingroups'])==0) && (strlen($_SESSION['offgroups'])==0)) {
					assignError('Such-String zu kurz','Suchwort muss mindestens 3 Zeichen lang sein');
				} else {
					$id_list=array();
					$table = new HTML_Table();
					$table->setCaption('Member-Suche');
					$table->setColCount(8);
					$table->setHeaderContents(0, 0, 'ID');
					$table->setColAttributes(0,'style="width:'.COLWIDTH_MAP.'px;" class="collapsable_4"');
					$table->setHeaderContents(0, 1, 'Nickname');
					$table->setHeaderContents(0, 2, 'Name');
					$table->setColAttributes(2,'style="width:'.COLWIDTH_NAME.'px; height:35px;"');
					$table->setHeaderContents(0, 3, 'e-Mail');
					$table->setColAttributes(3,'style="width:'.COLWIDTH_TECHC.'px;');
					$table->setHeaderContents(0, 4, 'Aktion');
					$table->setHeaderContents(0, 5, 'Member');
					$table->setHeaderContents(0, 6, '#Nodes<br>#TechC');
					$table->setColAttributes(6,'style="width:'.COLWIDTH_AAAA.'px;" class="collapsable_5"');
					$table->setHeaderContents(0, 7, 'changed');
					$table->setColAttributes(7,'style="class="collapsable_3"');
					$table->setHeaderContents(0, 8, 'last login');
					$table->setColAttributes(8,'style="class="collapsable_2"');
					$table->setHeaderContents(0, 9, 'created');
					$table->setColAttributes(9,'style="class="collapsable_2"');
					$credentials->isAdmin() ? $target="'#'||nickname||'#'||coalesce(firstname,'')||' '||coalesce(lastname,'')||' '||coalesce(firstname,'')||'#'||coalesce(street,'')||'#'||coalesce(email,'')||'#'||coalesce(telephone,'')||'#'||coalesce(mobilephone,'')||'#'" : $target="'#'||nickname||'#'||coalesce(firstname,'')||' '||coalesce(lastname,'')||' '||coalesce(firstname,'')||'#'||coalesce(email,'')||'#'||coalesce(telephone,'')||'#'||coalesce(mobilephone,'')||'#'";
					$groups="";
					if ((strlen($_SESSION['ingroups'])>0) && (preg_match("/[0-9,]/", $_SESSION['ingroups']))) {
						$groups.=" and m.id in (select id_members from members_roles where id_roles in (".$_SESSION['ingroups']."))";
					}
					if ((strlen($_SESSION['offgroups'])>0) && (preg_match("/[0-9,]/", $_SESSION['offgroups']))) {
						$groups.=" and m.id not in (select id_members from members_roles where id_roles in (".$_SESSION['offgroups']."))";
					}
					if (is_numeric($_SESSION['search'])) { $searchbyid=" or m.id=".ConvertUtils::escape($_SESSION['search'], 'string'); }
					else { $searchbyid=""; }
					$q="SELECT m.*, (select min(id_roles) from members_roles r where r.id_members=m.id and id_roles between 3 and 6) as state ".
						",(select min(id_roles) from members_roles r where r.id_members=m.id and id_roles between 1 and 2) as admin ".
						",(select min(id_roles) from members_roles r where r.id_members=m.id and id_roles=9) as deleteme ".
						",(select count(*) from nodes n where n.id_members=m.id and n.id>0) as nodecount ".
						",(select count(*) from nodes t where t.id_members!=m.id and t.id_tech_c=m.id and t.id>0) as techcount ".
						"from members m where (lower(".$target.") like lower(".ConvertUtils::escape('%'.$_SESSION['search'].'%', 'string').") ".
						$searchbyid.") ".
						$groups.
						" order by id ".
						(isset($_SESSION['limitresult']) ? " LIMIT ".MAX_LIMITED." " : " LIMIT ".MAX_NOLIMIT." ");
					$result = $databaseUtils->getRows($q);
					$c=0;
					$explain=array('0'=>'nix', '1'=>'Admin', '2'=>'SU', '3'=>'Ord.', '4'=>'AO', '5'=>'Kein', '6'=>'F&ouml;rd.', '7'=>'Ehren', '9'=>'Del');
					while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
						$c=$c+1;
						if (($credentials->isAdmin()) && ($row['nodecount']+$row['techcount'] ==0) && (($row['state'] == '') || ($row['state'] == '5')) && ($row['id']>=2)) {
							$deleteme='&nbsp;&nbsp;&nbsp;<a href="'.$_SERVER['PHP_SELF'].'?section=member&action=delete&id='.$row['id'].'" onclick="return askDelete()" title="Löschen"><i class="far fa-trash-alt fa-2x" ></i></a>';
						} else {
							$deleteme='&nbsp;&nbsp;&nbsp;<i class="far fa-trash-alt fa-2x" style="color:gray;"></i>';
						}
						$table->addRow(array($row['id'],
							$row['nickname'],
							$row['firstname']." ".$row['lastname'],
							'<div class="tooltip" id="resultcode_'.$row['id'].'" style="display: inline-block; padding-right:4px; white-space: normal;" OnMouseOver="javascript:checkit('.$row['id'].');"><i class="far fa-play-circle fa-lg" style="color:gray;"></i><span class="tooltiptext">nicht gepr&uuml;ft</span></div>'.
								'<div id="emailaddr_'.$row['id'].'" style="display: inline-block; padding-right:4px;">'.$row['email'].'</div><div id="checked_'.$row['id'].'" style="display: none">0</div>',
							'<center><a href="'.$PHP_SELF.'?section=member&action=update&id='.$row['id'].'"><i class="fas fa-user-edit fa-2x"></i></a>'.$deleteme.'</center>',
							'<center>'.$explain[$row['state']].' <font color=red>'.$explain[$row['admin']].'</font> <font color=blue>'.$explain[$row['deleteme']].'</font></center>',
							'<center>'.($row['nodecount']==0 ? '-' : $row['nodecount'])." / ".($row['techcount']==0 ? '-' : $row['techcount']).'</center>',
							format_dateonly($row['changed']),
							format_dateonly($row['last_login']),
							format_dateonly($row['created'])
						));
						$table->setColAttributes(3,'style="width:'.COLWIDTH_TECHC.'px; display:table-cell;vertical-align: middle; white-space: nowrap;"');
						$table->setColAttributes(0,'style="width:'.COLWIDTH_MAP.'px;" class="collapsable_4"');
						$table->setColAttributes(6,'style="width:'.COLWIDTH_AAAA.'px;" class="collapsable_5"');
						$table->setColAttributes(7,'class="collapsable_3"');
						$table->setColAttributes(8,'class="collapsable_2"');
						$id_list[]=$row['id'];
					}
					echo $table->toHtml();
					echo '<div class="tooltip"><span class="tooltiptext"></span></div>'; //this line is needed to make other tooltips work on MobileBrowsers (WTF!)
					if (($c >= MAX_LIMITED) && (isset($_SESSION['limitresult']))) echo "<br>Maximal ".MAX_LIMITED." Treffer werden angezeigt.";
					else if (($c >= MAX_NOLIMIT) && (!isset($_SESSION['limitresult']))) echo "<br>Maximal ".MAX_NOLIMIT." Treffer werden angezeigt.";
					else print "<br>".$c." Treffer gefunden.";
					$id_list_str=implode(',',$id_list);
			echo <<<EOT
<script>
function checkit(value) {
  var i=document.getElementById('checked_'+value);
  if (i.innerHTML!=='1') {
    i.innerHTML='1';
    setTimeout(function(){
      var d=document.getElementById('emailaddr_'+value);
      needCheck=true;
      lastVerified='x';
      checksmtp(d.innerHTML, value);
    }, 100);
  }
}
var all = [{$id_list_str}];
all.forEach(function(item){checkit(item);});
</script>
EOT;
				}
			}
		}

	function voip($id) {
		global $databaseUtils;

		if (checkRights($id, 'members')) {
			$table = new HTML_Table();
			$table->setCaption('VoIP [<a href="'.$_SERVER['REQUEST_URI'].'&action=insert">hinzuf&uuml;gen</a>]');
			$table->setColCount(10);
			$table->setHeaderContents(0, 0, 'Durchwahl');
			$table->setHeaderContents(0, 1, 'POTS');
			$table->setHeaderContents(0, 2, 'Voicemail');
			$table->setHeaderContents(0, 3, 'Voicemail PIN');
			$table->setHeaderContents(0, 4, 'Voicemail E-Mail Notify');
			$table->setHeaderContents(0, 5, 'Voicemail E-Mail Attachments');
			$table->setHeaderContents(0, 6, 'Voicemail Delay');
			$table->setHeaderContents(0, 7, 'E-Mail Notify');
			//$table->setHeaderContents(0, 8, 'H323');
			//$table->setHeaderContents(0, 9, 'H323 IP');
			$table->setHeaderContents(0, 8, 'Telefonbuch');
			//$table->setHeaderContents(0, 9, 'SIP-Devices');
			$table->setHeaderContents(0, 9, 'L&ouml;schen');
			$result = $databaseUtils->getRows('SELECT * FROM voip_extensions WHERE id_members='.ConvertUtils::escape($id, 'integer').' AND meetme=FALSE ORDER BY extension');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$table->addRow(array('<a href="'.$_SERVER['REQUEST_URI'].'&action=update&id_members='.$_GET['id_members'].'&id='.$row['id'].'" style="font-size:1.5em">'.$row['extension'].'</a>',
					$row['potsnumber'],
					ConvertUtils::buildCheckbox($row['voicemail']),
					$row['voicemail_pin'],
					ConvertUtils::buildCheckbox($row['voicemail_emailnotify']),
					ConvertUtils::buildCheckbox($row['voicemail_emailnotify_attach']),
					$row['voicemail_delay'],
					ConvertUtils::buildCheckbox($row['emailnotify']),
					//ConvertUtils::buildCheckbox($row['h323']),
					//$row['h323_ip'],
					ConvertUtils::buildCheckbox($row['phonebook']),
					//'<center><a href="'.$_SERVER['PHP_SELF'].'?section=voip_sip&id_voip_extensions='.$row['id'].'">SHOW</a></center>',
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=delete&id_members='.$_GET['id_members'].'&id='.$row['id'].'" onclick="return askDelete()">X</a></center>'
					),
					array(array(rowspan=>2),array(),array(),array(),array(),array(),array(),array(),array(),array(),array())
				);

				// create subtable for sip devices
				$t2 = new HTML_Table();
				$t2->setColCount(/* 11 */ 5);
				$t2->setHeaderContents(0, 0, 'Login');
				$t2->setHeaderContents(0, 1, 'Secret');
				$t2->setHeaderContents(0, 2, 'NAT');
				$t2->setHeaderContents(0, 3, 'DTMF Mode');
				$t2->setHeaderContents(0, /* 10 */ 4, 'L&ouml;schen');
				$result2 = $databaseUtils->getRows('SELECT voip_sip.*, members.nickname FROM voip_sip, voip_extensions, members WHERE voip_sip.id_voip_extensions='.ConvertUtils::escape($row[id], 'integer').' AND members.id=voip_extensions.id_members AND voip_extensions.id=voip_sip.id_voip_extensions ORDER BY id');
				$c2=0;
				while ($row2 = $result2->fetchRow(DB_FETCHMODE_ASSOC)) {
					$c2++;
					$t2->addRow(array('<a href="'.$_SERVER['PHP_SELF'].'?section=voip_sip&action=update&id_members='.$_GET['id_members'].'&id='.$row2['id'].'">'.$row2['nickname'].'_'.$row2['id'].'</a>',
						$row2['secret'],
						ConvertUtils::buildCheckbox($row2['nat']),
						$row2['dtmfmode'],
						'<center><a href="'.$_SERVER['PHP_SELF'].'?section=voip_sip&action=delete&id_members='.$_GET['id_members'].'&id='.$row2['id'].'" onclick="return askDelete()">X</a></center>'
					));
				} // inner while (sip devices)

				$table->setCellContents($table->getRowCount()-1,1,($c2?$t2->toHtml():"").
					"<a href='{$SERVER[PHP_SELF]}?section=voip_sip&action=insert&id_members={$_GET[id_members]}&id_voip_extensions={$row['id']}'>add sip device</a>"
				);
				$table->setCellAttributes($table->getRowCount()-1,1,array(colspan=>9,style=>"background-color:#e8e8e8"));

			} // while
			echo $table->toHtml();
		}
	}

	function meetme($id) {
		global $databaseUtils;

		if (checkRights($id, 'members')) {
			$table = new HTML_Table();
			$table->setCaption('MeetMe [<a href="'.$_SERVER['REQUEST_URI'].'&action=insert">hinzuf&uuml;gen</a>]');
			$table->setColCount(5);
			$table->setHeaderContents(0, 0, 'Durchwahl');
			$table->setHeaderContents(0, 1, 'POTS');
			$table->setHeaderContents(0, 2, 'PIN');
			$table->setHeaderContents(0, 3, 'Admin PIN');
			$table->setHeaderContents(0, 4, 'L&ouml;schen');
			$result = $databaseUtils->getRows('SELECT * FROM voip_extensions WHERE id_members='.ConvertUtils::escape($id, 'integer').' AND meetme=TRUE');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$table->addRow(array('<a href="'.$_SERVER['REQUEST_URI'].'&action=update&id='.$row['id'].'">'.$row['extension'].'</a>',
					$row['potsnumber'],
					$row['meetme_pin'],
					$row['meetme_admin_pin'],
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=delete&id='.$row['id'].'" onclick="return askDelete()">X</a></center>'
				));
			}
			echo $table->toHtml();
		}
	}

	function phonebook($id) {
		global $databaseUtils;

		if (checkRights($id, 'members')) {
			$table = new HTML_Table();
			$table->setCaption('Phonebook');
			$table->setColCount(3);
			$table->setHeaderContents(0, 0, 'Nachname');
			$table->setHeaderContents(0, 1, 'Vorname');
			$table->setHeaderContents(0, 2, 'Nummer');
			$result = $databaseUtils->getRows('SELECT members.lastname AS lastname, members.firstname AS firstname, voip_extensions.extension AS extension FROM voip_extensions,members WHERE voip_extensions.id_members=members.id AND voip_extensions.phonebook=TRUE ORDER BY members.lastname ASC');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$table->addRow(array($row['lastname'],
					$row['firstname'],
					$row['extension'],
				));
			}
			echo $table->toHtml();
		}
	}


	function voip_sip($id) {
		global $databaseUtils;

		if (checkRights($_POST['id'], 'members')) {
			// get the extention we are showing sip devices for
			$res2=$databaseUtils->getRows('SELECT * from voip_extensions where id='.ConvertUtils::escape($id, 'integer'));
			$row2=$res2->fetchRow(DB_FETCHMODE_ASSOC);

			$table = new HTML_Table();
			$table->setCaption('VoIP Extension '.$row2['extension'].' SIP [<a href="'.$_SERVER['REQUEST_URI'].'&action=insert">hinzuf&uuml;gen</a>] [<a href="'.$_SERVER['PHP_SELF'].'?section=voip&id_members='.$row2['id_members'].'">Zur&uuml;ck zu VOIP</a>]');
			$table->setColCount(/* 11 */ 6);
			$table->setHeaderContents(0, 0, 'Login');
			$table->setHeaderContents(0, 1, 'Secret');
			$table->setHeaderContents(0, 2, 'NAT');
			$table->setHeaderContents(0, 3, 'DTMF Mode');
			$table->setHeaderContents(0, 4, 'Port');
			/* $table->setHeaderContents(0, 5, 'Provider');
			$table->setHeaderContents(0, 6, 'Provider Host');
			$table->setHeaderContents(0, 7, 'Provider FromUser');
			$table->setHeaderContents(0, 8, 'Provider FromDomain');
			$table->setHeaderContents(0, 9, 'Provider Secret'); */
			$table->setHeaderContents(0, /* 10 */ 5, 'L&ouml;schen');
			$result = $databaseUtils->getRows('SELECT voip_sip.*, members.nickname FROM voip_sip, voip_extensions, members WHERE voip_sip.id_voip_extensions='.ConvertUtils::escape($id, 'integer').' AND members.id=voip_extensions.id_members AND voip_extensions.id=voip_sip.id_voip_extensions');
			while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
				$table->addRow(array('<a href="'.$_SERVER['REQUEST_URI'].'&action=update&id='.$row['id'].'">'.$row['nickname'].'_'.$row['id'].'</a>',
					$row['secret'],
					ConvertUtils::buildCheckbox($row['nat']),
					$row['dtmfmode'],
					$row['port'],
					/* ConvertUtils::buildCheckbox($row['provider']),
					$row['provider_host'],
					$row['provider_fromuser'],
					$row['provider_fromdomain'],
					$row['provider_secret'], */
					'<center><a href="'.$_SERVER['REQUEST_URI'].'&action=delete&id='.$row['id'].'" onclick="return askDelete()">X</a></center>'
				));
			}
			echo $table->toHtml();
		}
	}
}

?>

<?php

class DatabaseUtils {
	var $connection;

	var $my_stuff = array();

	function DatabaseUtils() {
		$dsn = 'pgsql://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB;
		$dboptions = array('debug'=>999);

		$db = DB::connect($dsn, $dboptions);
		//pg_set_client_encoding('WIN1250');
		//db is utf-8, (website encoding too)
		//pg_set_client_encoding('UTF-8');

		$this->connection = $db;
	}

	function getFirstRow($sql) {
		debugMessage($sql);
		$result = $this->connection->query($sql);
		if ($this->checkError($result))
			return $result->fetchRow(DB_FETCHMODE_ASSOC);
	}

	function getRows($sql) {
		debugMessage($sql);
		$result = $this->connection->query($sql);
		if ($this->checkError($result))
			return $result;
		return null;
	}

	function sendQuery($sql) {
		debugMessage($sql);
		return $this->checkError($this->connection->query($sql));
	}

	function checkError($obj) {
		if (is_a($obj, 'db_error')) {
			assignError($obj->message, $obj->userinfo);
			return false;
		}
		return true;
	}

	function reset() {
		debugMessage("in pwdreset");
		if (isset($_POST["CSRFtoken"], $_COOKIE["CSRFtoken"])) {
			if ($_POST["CSRFtoken"] == $_COOKIE["CSRFtoken"]) {
				//CSRFtoken is ok, so now go ahead with the reset request
				$query = 'SELECT id, nickname , email '.  'FROM members '.  'WHERE id > 0 AND ( nickname=LOWER('.ConvertUtils::escape($_POST['nickname'], 'string').') '.
					'OR LOWER(email)=LOWER(' .ConvertUtils::escape($_POST['email'], 'string').') )';//emails are not fully case-insensitive but in this case asuming this is better than not.
				$result = $this->getRows($query);
				$row = $result->fetchRow(DB_FETCHMODE_ASSOC);
				debugMessage("row= $row");
				$row1 = $result->fetchRow(DB_FETCHMODE_ASSOC);//verify if there is more than one result
				if ($row1['id'] > 0) { //We might find multiple users due to incansitive email search, or user entering both nickname and emailadress!
					if (($_POST['email'] != '') && ($_POST['nickname'] != ''))
						print "error: More than one matching user account found! Try using either nickname OR email, not both!<p>\n";
					else
						print 'error: More than one matching user account found! Please ask an admin for help on resolving this! ('.$_POST['nickname'].', '.$_POST['email'].")<p>\n";
					return false;
				} else if ($row['id'] > 0) {
					$id = $row['id'];
					$em = $row['email'];
					$n = $row['nickname'];
					$password = generatePassword();
					$this->sendQuery("UPDATE members set password=md5('". $password ."') where id=$id");
					debugMessage("mailing new pwd....");
					mailNewPassword($n, $password, $em);
					return true;
				}
				else {
					print "error: We do not know you in our database or the database query was broken<p>\n";
					return false;
				}
			} else {
				print "error: your login token might have been expired. Please go back, reload the page and try again<p>\n";
				return false; // token not correct
			}
		} else {
			print "error: your login token might have been expired. Please go back, reload the page and try again<p>\n";
			return false; // token missing
		}
	}


	function fetchMyStuff() {
		global $credentials;

		$this->my_stuff['members'] = array();
		$result = $this->getRows('SELECT id FROM members WHERE id='.$credentials->getId());
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC))
			array_push($this->my_stuff['members'], $row['id']);

		$this->my_stuff['owner'] = array();
		$result = $this->getRows('SELECT id FROM nodes WHERE id_members='.$credentials->getId().' UNION '.
								'SELECT id From nodes WHERE -id in (SELECT id FROM nodes WHERE id_members='.$credentials->getId().')');
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) 
			array_push($this->my_stuff['owner'], $row['id']);

		$this->my_stuff['nodes'] = array();
		$result = $this->getRows('SELECT id FROM nodes WHERE id_members='.$credentials->getId().' OR id_tech_c='.$credentials->getId().' UNION '.
								'SELECT id From nodes WHERE -id in (SELECT id FROM nodes WHERE id_members='.$credentials->getId().' OR id_tech_c='.$credentials->getId().')');
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC))
			array_push($this->my_stuff['nodes'], $row['id']);

		$this->my_stuff['devices'] = array();
		$result = $this->getRows('SELECT devices.id FROM nodes, devices WHERE ( nodes.id_members='.$credentials->getId().' OR id_tech_c='.$credentials->getId().' ) AND devices.id_nodes=nodes.id');
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC))
			array_push($this->my_stuff['devices'], $row['id']);

		$this->my_stuff['ip6'] = array();
		$result = $this->getRows('select i.id from ip6 i where i.id_nodes in (select n.id from nodes n  WHERE (n.id_members='.$credentials->getId().' OR n.id_tech_c='.$credentials->getId().')) union all '.
					'select i.id from ip6 i where i.id_devices in (SELECT d.id FROM nodes n, devices d WHERE (n.id_members='.$credentials->getId().' OR n.id_tech_c='.$credentials->getId().') and n.id=d.id_nodes)');
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC))
			array_push($this->my_stuff['ip6'], $row['id']);

		$this->my_stuff['voips'] = array();
		$result = $this->getRows('SELECT id FROM voip_extensions WHERE id_members='.$credentials->getId().' AND meetme=FALSE');
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC))
			array_push($this->my_stuff['voips'], $row['id']);

		$this->my_stuff['voip_sips'] = array();
		$result = $this->getRows('SELECT voip_sip.id FROM voip_extensions, voip_sip WHERE voip_extensions.id_members='.$credentials->getId().' AND voip_extensions.id=voip_sip.id_voip_extensions');
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC))
			array_push($this->my_stuff['voip_sips'], $row['id']);

		$this->my_stuff['meetmes'] = array();
		$result = $this->getRows('SELECT id FROM voip_extensions WHERE id_members='.$credentials->getId().' AND meetme=TRUE');
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC))
			array_push($this->my_stuff['meetmes'], $row['id']);
	}

	function insertMember() {
		global $credentials;
		if (empty($_POST['password'])) {
			$password = generatePassword();
		} else {
			$password = $_POST['password'];
		}

		if (($credentials->isAdmin()) || $credentials->isSuperuser()) {
			//validate formular field contents!
			$success = $this->sendQuery('SELECT * FROM insertmember(LOWER('.ConvertUtils::escape($_POST['nickname'], 'string').'),
				'.ConvertUtils::escape($password, 'string').',
				'.ConvertUtils::escape($_POST['firstname'], 'string').',
				'.ConvertUtils::escape($_POST['lastname'], 'string').',
				'.ConvertUtils::escape($_POST['street'], 'string').',
				'.ConvertUtils::escape($_POST['housenumber'], 'string').',
				'.ConvertUtils::escape($_POST['zip'], 'string').',
				'.ConvertUtils::escape($_POST['town'], 'string').',
				'.ConvertUtils::escape($_POST['telephone'], 'string').',
				'.ConvertUtils::escape($_POST['mobilephone'], 'string').',
				'.ConvertUtils::escape($_POST['fax'], 'string').',
				'.ConvertUtils::escape($_POST['email'], 'string').',
				'.ConvertUtils::escape($_POST['homepage'], 'string').')');
			if ($success) {
				memberSubscribeMail($_POST['email'], $_POST['nickname'], $password);
				//if $_POST['role']  -> check or change roles 3..7
				//if $_POST['admin'] -> check or change roles 1..2
			}
		}
	}

	function updateMember() {
		global $credentials;
		if (($credentials->isAdmin()) && (ConvertUtils::escape($_POST['id'], 'integer')>0) && ($_POST['transform']=='go')) {
			$row = $this->getFirstRow('SELECT id, nickname, firstname '.
				'FROM members '.
				'WHERE id= '.ConvertUtils::escape($_POST['id'], 'integer'));
			if (!empty($row['id'])) {
				if (!isset($_SESSION['original_id'])) {
					/*was: self, becomes $id*/
					$_SESSION['original_id']=$credentials->getId();
					$_SESSION['original_nn']=$credentials->getNickname();
					$_SESSION['original_fn']=$credentials->getFirstname();
				} else if ($_SESSION['original_id']==$row['id']) {
					/*was someone else, now becomes self again*/
					unset($_SESSION['original_id']);
					unset($_SESSION['original_nn']);
					unset($_SESSION['original_fn']);
				}
				$credentials->id = $row['id'];
				$credentials->nickname = $row['nickname'];
				$credentials->firstname= $row['firstname'];
				storeCredentials();
				echo "<script>window.location='".$_SERVER['PHP_SELF']."?section=nodes';</script>";
				echo "</div></body></html>";
				exit;
			}
		}
		if (($credentials->isAdmin()) || (checkRights($_POST['id'], 'members'))) {
			$curr=$this->getFirstRow('SELECT * from members where id='.ConvertUtils::escape($_POST['id'], 'integer'));
			if ($credentials->isAdmin()) {
				$firstname = $_POST['firstname'];
				$lastname = $_POST['lastname'];
			} else {
				$firstname = $curr['firstname'];
				$lastname = $curr['lastname'];
			}
			$this->sendQuery('SELECT * FROM updatemember('.ConvertUtils::escape($_POST['id'], 'integer').',
				LOWER(\''.$curr['nickname'].'\'),
				'.ConvertUtils::escape($_POST['password'], 'string').',
				'.ConvertUtils::escape($firstname, 'string').',
				'.ConvertUtils::escape($lastname, 'string').',
				'.ConvertUtils::escape($_POST['street'], 'string').',
				'.ConvertUtils::escape($_POST['housenumber'], 'string').',
				'.ConvertUtils::escape($_POST['zip'], 'string').',
				'.ConvertUtils::escape($_POST['town'], 'string').',
				'.ConvertUtils::escape($_POST['telephone'], 'string').',
				'.ConvertUtils::escape($_POST['mobilephone'], 'string').',
				'.ConvertUtils::escape($_POST['fax'], 'string').',
				'.ConvertUtils::escape($_POST['email'], 'string').',
				'.ConvertUtils::escape($_POST['homepage'], 'string').')');
		} else if (($credentials->isSuperuser()) && ($_POST['mini']=='true')) {
			/*first get old data for non-update-fields*/
			$curr=$this->getFirstRow('SELECT * from members where id='.ConvertUtils::escape($_POST['id'], 'integer'));
			$this->sendQuery('SELECT * FROM updatemember('.$curr['id'].',
				LOWER(\''.$curr['nickname'].'\'),
				null,
				\''.$curr['firstname'].'\',
				\''.$curr['lastname'].'\',
				\''.$curr['street'].'\',
				\''.$curr['housenumber'].'\',
				\''.$curr['zip'].'\',
				\''.$curr['town'].'\',
				'.ConvertUtils::escape($_POST['telephone'], 'string').',
				'.ConvertUtils::escape($_POST['mobilephone'], 'string').',
				\''.$curr['fax'].'\',
				'.ConvertUtils::escape($_POST['email'], 'string').',
				\''.$curr['homepage'].'\')');
		}
		foreach (USER_ROLES as $conf=>$set) {
			if (($set[1]==1) && (!$credentials->isAdmin())) continue;
			if (($set[1]==2) && (!$credentials->isSuperuser())) continue;
			//non-admins and non-superusers can only manage themselves
			$test=$this->getFirstRow('select id_roles id from members_roles where id_roles='.$set[1].' and id_members='.ConvertUtils::escape($_POST['id'], 'integer').' and id_members='.$credentials->getId());
			if ((!$test) && ($set[1]>2)) continue;
			if ((isset($_SESSION['original_id'])) && ($set[1]>2)) continue;

			$roles=explode(',',$set[2]);
			if (count($roles)==1) { // checkbox
				$curr=$this->getFirstRow('SELECT count(*) anz, min(id_roles) id from members_roles where id_roles='.$set[2].' and id_members='.ConvertUtils::escape($_POST['id'], 'integer'));
				if (($curr['anz'] == 1) && (ConvertUtils::escape($_POST['autoselect_'.$conf], 'boolean') == 'false')) {
					$this->sendQuery('DELETE FROM members_roles where id_roles='.$set[2].' and id_members='.ConvertUtils::escape($_POST['id'], 'integer'));
				} else if (($curr['anz'] == 0) && (ConvertUtils::escape($_POST['autoselect_'.$conf], 'boolean') == 'true')) {
					$this->sendQuery('INSERT INTO members_roles (id_roles, id_members) values ('.$set[2].','.ConvertUtils::escape($_POST['id'], 'integer').')');
				}
			} elseif (count($roles)>1) {//selection
				$sel=ConvertUtils::escape(str_replace('d','',$_POST['autoselect_'.$conf]), 'integer');
				$curr=$this->getFirstRow('SELECT count(*) anz, min(id_roles) id from members_roles where id_roles in ('.$set[2].') and id_members='.ConvertUtils::escape($_POST['id'], 'integer'));
				if (($curr['anz'] > 1) || ($curr['id'] != $sel)) {
					$this->sendQuery('DELETE FROM members_roles where id_roles in ('.$set[2].') and id_roles!='.$sel.' and id_members='.ConvertUtils::escape($_POST['id'], 'integer'));
					if (in_array($sel, $roles, false)) {
						$this->sendQuery('INSERT INTO members_roles (id_roles, id_members) select '.$sel.', '.ConvertUtils::escape($_POST['id'], 'integer').
							' where not exists (select 1 from members_roles where id_roles!=0 and id_roles='.$sel.' and id_members='.ConvertUtils::escape($_POST['id'], 'integer').')');
					}
				} else if (($curr['anz'] == 0) && (in_array($sel, $roles, false))) {
						$this->sendQuery('INSERT INTO members_roles (id_roles, id_members) select id, '.ConvertUtils::escape($_POST['id'], 'integer').
							' from roles '.
							' where id!=0 and id='.$sel.' and not exists (select 1 from members_roles where id_roles='.$sel.' and id_members='.ConvertUtils::escape($_POST['id'], 'integer').')');
				}
			}
		}
	}

	function deleteMember() {
		global $credentials;
		if ($credentials->isAdmin()) {
			$curr=$this->getFirstRow('SELECT count(*) anzahl FROM members_roles where id_members='.ConvertUtils::escape($_GET['id'], 'integer').' and id_roles in (1,2,3,4,5,6,7,8)');
			$nodes=$this->getFirstRow('SELECT count(*) anzahl FROM nodes where '.ConvertUtils::escape($_GET['id'], 'integer').' in (id_members, id_tech_c)');
			if ($curr['anzahl'] > 0) {
				assignError('Fehler - dieser User hat noch einen Mitgliedsstatus! Bitte zuerst Vereinszugehörigkeit auflösen');
			} elseif ($nodes['anzahl'] > 0) {
				assignError('Fehler - dieser User hat noch Nodes als Besitzer bzw. Tech-C zugeordnet');
			} else {
				$this->sendQuery('SELECT * FROM deletemember('.ConvertUtils::escape($_GET['id'], 'integer').')');
			}
		} else {
			assignError('Fehlende Rechte - nur Admins können User löschen');
		}
	}

	function insertNode() {
		if (checkRights($_POST['id_members'], 'members')) {
			if (!is_valid_domain_name('irgendwas.'.$_POST['name'].'.wien.funkfeuer.at')) {
				assignError('Dieser Node-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} else {
			$this->sendQuery('SELECT * FROM insertnode('.ConvertUtils::escape($_POST['id_members'], 'integer').',
				'.ConvertUtils::escape($_POST['name'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lat_deg'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lat_min'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lat_sec'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lon_deg'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lon_min'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lon_sec'], 'string').',
				'.ConvertUtils::escape($_POST['map'], 'boolean').')');
			}
		}
	}

	function updateNode() {
		if (checkRights($_POST['id'], 'nodes')) {
			if (!is_valid_domain_name('irgendwas.'.$_POST['name'].'.wien.funkfeuer.at')) {
				assignError('Dieser Node-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} else {
			$this->sendQuery('SELECT * FROM updatenode('.ConvertUtils::escape($_POST['id'], 'integer').',
				'.ConvertUtils::escape($_POST['name'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lat_deg'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lat_min'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lat_sec'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lon_deg'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lon_min'], 'string').',
				'.ConvertUtils::escape($_POST['gps_lon_sec'], 'string').',
				'.ConvertUtils::escape($_POST['map'], 'boolean').')');
			}

		}
	}

	function deleteNode() {
		if (checkRights($_GET['id'], 'nodes')) {
			$this->sendQuery('SELECT * FROM deletenode('.ConvertUtils::escape($_GET['id'], 'integer').')');
			$this->sendQuery('DELETE FROM nodes WHERE id < 0 AND id='.ConvertUtils::escape(-$_GET['id'],'integer').';');
		}
	}

	function acceptMemberChange() {
		$id=ConvertUtils::escape($_GET['id'], 'integer');
		if (checkRights(-$_GET['id'], 'nodes')) {
			//!!?? update created tiemstamp if id_members changes?
			$this->sendQuery('UPDATE nodes SET id_tech_c=(SELECT id_tech_c FROM nodes WHERE id=-'.$id.'), id_members=(SELECT id_members FROM nodes WHERE id=-'.$id.')  WHERE id='.$id.';');
			$this->sendQuery('DELETE FROM nodes WHERE id < 0 AND id=-'.$id.';');
		}
	}

	function declineMemberChange() {
		if (checkRights(-$_GET['id'], 'nodes')) {
			$this->sendQuery('DELETE FROM nodes WHERE id < 0 AND id='.ConvertUtils::escape(-$_GET['id'],'integer').';');
		}
	}

	//either moves node to other member, or assigns new techC
	function proposeMember($mode="techc") {
		global $credentials;
		if (checkRights($_GET['id'], 'owner')) {
			$techc=$_POST['id_tech_c'];
			if (is_numeric($techc)) {
				if ($techc < 0) $techc*=-1;
				$result=$this->getFirstRow('SELECT id, firstname, lastname, email from members WHERE id='.ConvertUtils::escape($techc,'integer'));
				$type='dieser ID';
			}
			else {
				if (strpos($techc,'@')!==false) {
					$result=$this->getFirstRow('SELECT id, firstname, lastname, email from members WHERE lower(email)='.ConvertUtils::escape(strtolower($techc),'string'));
					$type='dieser email';
				} else {
					$result=$this->getFirstRow('SELECT id, firstname, lastname, email from members WHERE nickname=LOWER('.ConvertUtils::escape($techc,'string').')');
					$type='diesem Nickname';
				}
			}
			if (!$result) {
				echo 'Kein Benutzer mit '.$type.' gefunden.';
			} else if (!empty($_GET['id'])) {
				$this->sendQuery('DELETE FROM nodes WHERE id < 0 AND id='.ConvertUtils::escape(-$_GET['id'],'integer').';');
				if ($mode=="owner") {
					if ($_POST['stay_tech_c']) $this->sendQuery('INSERT INTO "nodes" VALUES ('.ConvertUtils::escape(-$_GET['id'],'integer').', '.ConvertUtils::escape(-$_GET['id'],'string').', NULL, NULL, NULL, NULL, NULL, NULL, FALSE, '.$result['id'].', NULL, NULL, '.$credentials->getId().');');
					else $this->sendQuery('INSERT INTO "nodes" VALUES ('.ConvertUtils::escape(-$_GET['id'],'integer').', '.ConvertUtils::escape(-$_GET['id'],'string').', NULL, NULL, NULL, NULL, NULL, NULL, FALSE, '.$result['id'].', NULL, NULL, '.$result['id'].');');
				} else $this->sendQuery  ('INSERT INTO "nodes" VALUES ('.ConvertUtils::escape(-$_GET['id'],'integer').', '.ConvertUtils::escape(-$_GET['id'],'string').', NULL, NULL, NULL, NULL, NULL, NULL, FALSE, '.$credentials->getId().', NULL, NULL, '.$result['id'].');');
				echo 'Vorschlag erfolgreich gespeichert, <a href="mailto:'.$result['email'].'">'.$result['firstname'].' '.$result['lastname'].'</a> braucht ihn nun nur noch in seinem Account akzeptieren!';
			}

			echo '<br><a href=?section=nodes>Zur&uuml;ck zur Knotenliste</a>';
		}
	}

	function resignFromNode() {
		if (checkRights($_GET['id'], 'nodes')) {
			if ($_GET['p']) {
				$this->sendQuery('DELETE FROM nodes WHERE id < 0 AND id_tech_c='.ConvertUtils::escape($_GET['p'],'integer').' and id='.ConvertUtils::escape(-$_GET['id'],'integer').';');
			} else {
				$this->sendQuery('UPDATE nodes SET id_tech_c=id_members WHERE id_tech_c!=id_members and id = ('.ConvertUtils::escape($_GET['id'], 'integer').')');
			}
		}
	}

	function insertDevice() {
		if (checkRights($_POST['id_nodes'], 'nodes')) {
			if (!is_valid_domain_name($_POST['name'].'.irgendwas.wien.funkfeuer.at')) {
				assignError('Dieser Device-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} else {
			$this->sendQuery('SELECT * FROM insertdevice('.ConvertUtils::escape($_POST['id_nodes'], 'integer').',
				'.ConvertUtils::escape($_POST['name'], 'string').',
				'.ConvertUtils::escape($_POST['antenna'], 'string').',
				'.ConvertUtils::escape($_POST['hardware'], 'string').',
				'.ConvertUtils::escape($_POST['ssid'], 'string').',
				'.ConvertUtils::escape($_POST['mac'], 'string').',
				'.ConvertUtils::escape($_POST['smokeping'], 'boolean').')');
			}
		}
	}

	function updateDevice() {
		if (checkRights($_POST['id'], 'devices')) {
			if (!is_valid_domain_name($_POST['name'].'.irgendwas.wien.funkfeuer.at')) {
				assignError('Dieser Device-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} else {
			$this->sendQuery('SELECT * FROM updatedevice('.ConvertUtils::escape($_POST['id'], 'integer').',
				'.ConvertUtils::escape($_POST['name'], 'string').',
				'.ConvertUtils::escape($_POST['antenna'], 'string').',
				'.ConvertUtils::escape($_POST['hardware'], 'string').',
				'.ConvertUtils::escape($_POST['ssid'], 'string').',
				'.ConvertUtils::escape($_POST['mac'], 'string').',
				'.ConvertUtils::escape($_POST['smokeping'], 'boolean').',
				'.ConvertUtils::escape($_POST['nodemaster'], 'boolean').')');
			}
		}
	}

	function moveDevice() {
		if ($_POST['what'] == 'IPv4') {
			if ( (checkRights($_GET['id'], 'devices')) && (checkRights($_POST['id_nodes'], 'nodes')) ){
				//let ip6 remain on this node
				$this->sendQuery('UPDATE ip6 set id_devices=null, id_nodes=(select id_nodes from devices where id='.ConvertUtils::escape($_GET['id'], 'integer').') where id_devices='.ConvertUtils::escape($_GET['id'], 'integer'));
				$this->sendQuery('UPDATE devices SET nodemaster=false, id_nodes=('.ConvertUtils::escape($_POST['id_nodes'], 'integer').') WHERE id=('.ConvertUtils::escape($_GET['id'], 'integer').')');
			}
		} else if ($_POST['what'] == 'IPv6') {
			$check=$this->getFirstRow('SELECT ip6 FROM ip6 WHERE id='.ConvertUtils::escape($_GET['id'], 'integer'));	
			if ( (checkIp6Range($check['ip6'],0,$_POST['id_nodes'])) && (checkRights($_GET['id'], 'ip6')) && (checkRights($_POST['id_nodes'], 'nodes')) ){
				//unhook ipv4 on this node
				$this->sendQuery('UPDATE ip6 u set id_devices=null, id_nodes=(select id_nodes from devices where id=u.id_devices) where id='.ConvertUtils::escape($_GET['id'], 'integer'));
				$this->sendQuery('UPDATE ip6 SET nodemaster=false, id_nodes=('.ConvertUtils::escape($_POST['id_nodes'], 'integer').') WHERE id=('.ConvertUtils::escape($_GET['id'], 'integer').')');
			}
		}
	}

	function deleteDevice() {
		if (checkRights($_GET['id'], 'devices')) {
			//let ip6 remain on this node
			$this->sendQuery('UPDATE ip6 set id_devices=null, id_nodes=(select id_nodes from devices where id='.ConvertUtils::escape($_GET['id'], 'integer').') where id_devices='.ConvertUtils::escape($_GET['id'], 'integer'));
			$this->sendQuery('SELECT * FROM deletedevice('.ConvertUtils::escape($_GET['id'], 'integer').')');
		}
	}

	function insertv642() {
		if (checkRights($_POST['id_nodes'], 'nodes')) {
			if (!is_valid_domain_name($_POST['name'].'.irgendwas.wien.funkfeuer.at')) {
				assignError('Dieser Device-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} else {
			$this->sendQuery('SELECT * FROM insertv642  ('.ConvertUtils::escape($_POST['id_nodes'], 'integer').',
				'.ConvertUtils::escape($_POST['id_ips'], 'integer').',
				'.ConvertUtils::escape($_POST['name'], 'string').',
				'.ConvertUtils::escape($_POST['smokeping'], 'boolean').',
				'.ConvertUtils::escape($_POST['dns_forward'], 'boolean').',
				'.ConvertUtils::escape($_POST['dns_reverse'], 'boolean').')');
			}
		}
	}

	function updatev642() {
		if (checkRights($_POST['id'], 'devices')) {
			if (!is_valid_domain_name($_POST['name'].'.irgendwas.wien.funkfeuer.at')) {
				assignError('Dieser Device-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} else {
			$this->sendQuery('SELECT * FROM updatev642  ('.ConvertUtils::escape($_POST['id'], 'integer').',
				'.ConvertUtils::escape($_POST['id_ips'], 'integer').',
				'.ConvertUtils::escape($_POST['name'], 'string').',
				'.ConvertUtils::escape($_POST['smokeping'], 'boolean').',
				'.ConvertUtils::escape($_POST['dns_forward'], 'boolean').',
				'.ConvertUtils::escape($_POST['dns_reverse'], 'boolean').',
				'.ConvertUtils::escape($_POST['nodemaster'], 'boolean').')');
			}
		}
	}

	function deletev642() {
		if (checkRights($_GET['id'], 'devices')) {
			$this->sendQuery('SELECT * FROM deletev642('.ConvertUtils::escape($_GET['id'], 'integer').')');
		}
	}

	function insertDevice6() {
		$ident=explode('-',$_POST['ids']);
		if ($ident[0]=='d') { // need to check node-id from this device
			$n=$this->getFirstRow('SELECT id_nodes from devices WHERE id='.ConvertUtils::escape($ident[1],'string'));
			if (($n['id_nodes']>0) && ($n['id_nodes']<999999999)) {$no=$n['id_nodes'];} else { $no=-1; }
		} else if ($ident[0]=='n') {
			$no=$ident[1];
		} else {
			$no=0;
		}
		if ((checkRights($_POST['id_nodes'], 'nodes')) && (checkRights($no, 'nodes'))) {
			if (!is_valid_domain_name($_POST['name'].'.irgendwas.wien.funkfeuer.at')) {
				assignError('Dieser Device-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} elseif (!checkIp6Range($_POST['ip6'],0,$no)) {
				/*Fehler kommt via checkIp6Range mit genauerem Text*/
				assignError('Fehlende Node-Rechte: '.$no);
			} else {
				$this->sendQuery('SELECT * FROM insertip6('.ConvertUtils::escape($ident[0], 'string').',
					'.ConvertUtils::escape($ident[1], 'integer').',
					'.ConvertUtils::escape($_POST['ip6'], 'string').',
					'.ConvertUtils::escape($_POST['name'], 'string').',
					'.ConvertUtils::escape($_POST['smokeping'], 'boolean').',
					'.ConvertUtils::escape($_POST['dns_forward'], 'boolean').',
					'.ConvertUtils::escape($_POST['dns_reverse'], 'boolean').',
					'.ConvertUtils::escape($_POST['antenna'], 'string').',
					'.ConvertUtils::escape($_POST['hardware'], 'string').',
					'.ConvertUtils::escape($_POST['ssid'], 'string').',
					'.ConvertUtils::escape($_POST['mac'], 'string').')'
				);
			}
		} else {
			assignError('Fehlende Rechte: '.$_POST['id_nodes'].'/'.$no);
		}
	}

	function updateDevice6() {
		$ident=explode('-',$_POST['ids']);
		if ($ident[0]=='d') { // need to check node-id from this device
			$no=$this->getFirstRow('SELECT id_nodes from devices WHERE id='.ConvertUtils::escape($ident[1],'string'));
			if (($no['id_nodes']>0) && ($no['id_nodes']<999999999)) { $no=$no['id_nodes']; } else { $no=-1; }
		} else if ($ident[0]=='n') {
			$no=$ident[1];
		} else {
			$no=0;
		}
		if ((checkRights($_POST['id'], 'ip6')) && (checkRights($no, 'nodes'))) {
			if (!is_valid_domain_name($_POST['name'].'.irgendwas.wien.funkfeuer.at')) {
				assignError('Dieser Device-Name verursacht einen ung&uuml;ltigen Domain-Name: '.$_POST['name']);
			} else if (!checkIp6Range($_POST['ip6'],0,$no)) {
				/*Fehler kommt via checkIp6Range mit genauerem Text*/
			} else {
				if ($ident[0] == 'n' ) { $id_nodes=$ident[1]; $id_devices=""; }
				else if ($ident[0] == 'd' ) { $id_nodes=""; $id_devices=$ident[1]; }
				else { $id_nodes=""; $id_devices=""; }
				$this->sendQuery('SELECT * FROM updateip6   ('.ConvertUtils::escape($_POST['id'], 'integer').',
					'.ConvertUtils::escape($id_nodes, 'integer').',
					'.ConvertUtils::escape($id_devices, 'integer').',
					'.ConvertUtils::escape($_POST['ip6'], 'string').',
					'.ConvertUtils::escape($_POST['name'], 'string').',
					'.ConvertUtils::escape($_POST['smokeping'], 'boolean').',
					'.ConvertUtils::escape($_POST['dns_forward'], 'boolean').',
					'.ConvertUtils::escape($_POST['dns_reverse'], 'boolean').',
					'.ConvertUtils::escape($_POST['antenna'], 'string').',
					'.ConvertUtils::escape($_POST['hardware'], 'string').',
					'.ConvertUtils::escape($_POST['ssid'], 'string').',
					'.ConvertUtils::escape($_POST['mac'], 'string').',
					'.ConvertUtils::escape($_POST['nodemaster'], 'boolean').')'
				);
			}
		} else {
			assignError('Fehlende Rechte');
		}
	}

	function deleteDevice6() {
		if (checkRights($_GET['id'], 'ip6')) {
			$this->sendQuery('SELECT * FROM deleteip6   ('.ConvertUtils::escape($_GET['id'], 'integer').')');
		}
	}

	function insertVoip() {
		if (checkRights($_POST['id_members'], 'members')) {
			$this->sendQuery('SELECT * FROM insertvoip('.ConvertUtils::escape($_POST['id_members'], 'integer').',
				'.ConvertUtils::escape($_POST['voicemail'], 'boolean').',
				'.ConvertUtils::escape($_POST['voicemail_pin'], 'string').',
				'.ConvertUtils::escape($_POST['voicemail_emailnotify'], 'boolean').',
				'.ConvertUtils::escape($_POST['voicemail_emailnotify_attach'], 'boolean').',
				'.ConvertUtils::escape($_POST['voicemail_delay'], 'integer').',
				'.ConvertUtils::escape($_POST['emailnotify'], 'boolean').',
				'.ConvertUtils::escape($_POST['h323'], 'boolean').',
				'.ConvertUtils::escape($_POST['h323_ip'], 'string').',
				'.ConvertUtils::escape($_POST['phonebook'], 'boolean').')');
		}
	}

	function updateVoip() {
		if (checkRights($_POST['id'], 'voips')) {
			$this->sendQuery('SELECT * FROM updatevoip('.ConvertUtils::escape($_POST['id'], 'integer').',
				'.ConvertUtils::escape($_POST['voicemail'], 'boolean').',
				'.ConvertUtils::escape($_POST['voicemail_pin'], 'string').',
				'.ConvertUtils::escape($_POST['voicemail_emailnotify'], 'boolean').',
				'.ConvertUtils::escape($_POST['voicemail_emailnotify_attach'], 'boolean').',
				'.ConvertUtils::escape($_POST['voicemail_delay'], 'integer').',
				'.ConvertUtils::escape($_POST['emailnotify'], 'boolean').',
				'.ConvertUtils::escape($_POST['h323'], 'boolean').',
				'.ConvertUtils::escape($_POST['h323_ip'], 'string').',
				'.ConvertUtils::escape($_POST['phonebook'], 'boolean').')');
		}
	}

	function deleteVoip() {
		if (checkRights($_GET['id'], 'voips')) {
			$this->sendQuery('SELECT * FROM deletevoip('.ConvertUtils::escape($_GET['id'], 'integer').')');
		}
	}

	function insertMeetme() {
		if (checkRights($_POST['id_members'], 'members')) {
			$this->sendQuery('SELECT * FROM insertmeetme('.ConvertUtils::escape($_POST['id_members'], 'integer').',
				'.ConvertUtils::escape($_POST['meetme_pin'], 'string').',
				'.ConvertUtils::escape($_POST['meetme_admin_pin'], 'string').')');
		}
	}

	function updateMeetme() {
		if (checkRights($_POST['id'], 'meetmes')) {
			$this->sendQuery('SELECT * FROM updatemeetme('.ConvertUtils::escape($_POST['id'], 'integer').',
				'.ConvertUtils::escape($_POST['meetme_pin'], 'string').',
				'.ConvertUtils::escape($_POST['meetme_admin_pin'], 'string').')');
		}
	}

	function deleteMeetme() {
		if (checkRights($_GET['id'], 'meetmes')) {
			$this->sendQuery('SELECT * FROM deletemeetme('.ConvertUtils::escape($_GET['id'], 'integer').')');
		}
	}

	function insertVoipSip() {
		if (checkRights($_POST['id_voip_extensions'], 'voips')) {
			$this->sendQuery('SELECT * FROM insertvoipsip('.ConvertUtils::escape($_POST['id_voip_extensions'], 'integer').',
				'.ConvertUtils::escape($_POST['secret'], 'string').',
				'.ConvertUtils::escape($_POST['nat'], 'boolean').',
				'.ConvertUtils::escape($_POST['dtmfmode'], 'string').',
				'.ConvertUtils::escape($_POST['port'], 'integer').',
				'.ConvertUtils::escape($_POST['provider'], 'boolean').',
				'.ConvertUtils::escape($_POST['provider_host'], 'string').',
				'.ConvertUtils::escape($_POST['provider_fromuser'], 'string').',
				'.ConvertUtils::escape($_POST['provider_fromdomain'], 'string').',
				'.ConvertUtils::escape($_POST['provider_secret'], 'string').')');
		}
	}

	function updateVoipSip() {
		if (checkRights($_POST['id'], 'voip_sips')) {
			$this->sendQuery('SELECT * FROM updatevoipsip('.ConvertUtils::escape($_POST['id'], 'integer').',
				'.ConvertUtils::escape($_POST['secret'], 'string').',
				'.ConvertUtils::escape($_POST['nat'], 'boolean').',
				'.ConvertUtils::escape($_POST['dtmfmode'], 'string').',
				'.ConvertUtils::escape($_POST['port'], 'integer').',
				'.ConvertUtils::escape($_POST['provider'], 'boolean').',
				'.ConvertUtils::escape($_POST['provider_host'], 'string').',
				'.ConvertUtils::escape($_POST['provider_fromuser'], 'string').',
				'.ConvertUtils::escape($_POST['provider_fromdomain'], 'string').',
				'.ConvertUtils::escape($_POST['provider_secret'], 'string').')');
		}
	}

	function deleteVoipSip() {
		if (checkRights($_GET['id'], 'voip_sips')) {
			$this->sendQuery('SELECT * FROM deletevoipsip('.ConvertUtils::escape($_GET['id'], 'integer').')');
		}
	}

	function welcome() {
		global $credentials;

		echo 'Willkommen im Redeemer!<br><br>';

		$result = $this->getRows('SELECT * FROM members_roles WHERE id_roles BETWEEN 2 AND 10 AND id_members='.$credentials->getId().' ORDER by id_roles DESC;');
		$mitglied=array();
		$link='<a href="https://wiki.funkfeuer.at/images/f/fb/Mitgliedsantrag-Funkfeuer-Wien.pdf" target="_new">Antragsformular</a>';
		while ($row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
			if ($row['id_roles']==3) $mitglied[]='ordentliches Mitglied';// echo 'Du bist ordentliches Vereinsmitglied von FunkFeuer Wien!<br/><br/>';
			if ($row['id_roles']==4) $mitglied[]='ausserord. Mitglied';// echo 'Du bist Vereinsmitglied von FunkFeuer Wien!<br/><br/>';
			if ($row['id_roles']==6) $mitglied[]='f&ouml;derndes Mitglied';// echo 'Du bist f&ouml;rderndes Vereinsmitglied von FunkFeuer Wien!<br/><br/>';
			if ($row['id_roles']==5) $mitglied[]='kein Mitglied';// echo 'F&uuml;r diesen User ist keine Vereinsmitgliedschaft von FunkFeuer Wien eingetragen!<br/><br/>';
			if ($row['id_roles']==7) $mitglied[]='Ehrenmitglied';// echo 'Du bist Ehrenmitglied von FunkFeuer Wien!<br/><br/>';
			//if no-member show link to mitgliedsantragsformular
		}
		if (count($mitglied) == 0) { $mitglied[]='unbestimmt<br>'.$link; }
		else if (in_array(min($mitglied), array('ordentliches Mitglied','ausserord. Mitglied','Ehrenmitglied'))) {
			$vereinmitglieder=$this->getFirstRow(
				'select (select count(*) from members_roles m where not exists (select 1 from members_roles d where d.id_roles in (7) and d.id_members=m.id_members) and id_roles=3) as  ordentliche, '.
				'(select count(*) from members_roles m where id_roles=7) as  ehren, '.
				'(select count(*) from members_roles m where not exists (select 1 from members_roles d where d.id_roles in (3,7) and d.id_members=m.id_members) and id_roles=4) as  ausserordentliche'
			);
			$text_m = 'Der Verein besteht derzeit aus '.($vereinmitglieder['ordentliche']+$vereinmitglieder['ausserordentliche']+$vereinmitglieder['ehren']).' Mitgliedern: ';
			$text_m.= $vereinmitglieder['ordentliche'].($vereinmitglieder['ordentliche']==1 ? ' ordentliches, ' : ' ordentliche, ');
            $text_m.= $vereinmitglieder['ausserordentliche'].($vereinmitglieder['ausserordentliche']==1 ? ' ausserordentliches und ' : ' ausserordentliche und ');
            $text_m.= $vereinmitglieder['ehren'].($vereinmitglieder['ehren']==1 ? ' Ehrenmitglied.<br>' : ' Ehrenmitglieder.<br>');
		}
		$row = $this->getFirstRow('SELECT * from members where id='.$credentials->getId());
		echo 'Deine Daten, '.$row['firstname'] ." ". $row['lastname'];
		$table = new HTML_Table();
		$table->setColCount(3);
		$table->setHeaderContents(0, 0, 'Mitglieds-Status');
		$table->setColAttributes(0,'style="width:'.COLWIDTH_TECHC.'px; height:35px;"');
		$table->setHeaderContents(0, 1, 'E-Mail-Adresse');
		$table->setColAttributes(1,'style="width:'.COLWIDTH_IPV6.'px; height:35px;"');
		$table->setHeaderContents(0, 2, 'Telefon');
		$table->setColAttributes(2,'style="width:'.COLWIDTH_NAME.'px; height:35px;"');
		$table->addRow(array(implode('<br>',$mitglied),
			'<div class="tooltip" id="resultcode_'.$row['id'].'" style="display: inline-block; padding-right:4px; white-space: normal;" onMouseOver="javascript:checkme();"><i class="far fa-question-circle fa-lg" style="color:gray;"></i><span class="tooltiptext">nicht gepr&uuml;ft</span></div>'.
				'<div id="emailaddr_'.$row['id'].'" style="display: inline-block; padding-right:4px;">'.$row['email'].'</div><div id="checked_'.$row['id'].'" style="display: none">0</div>',
			implode('<br>', array($row['mobilephone'],$row['telephone']))
		));
		$table->setColAttributes(1,'style="width:'.COLWIDTH_IPV6.'px; height:35px; white-space: nowrap;"');
		echo $table->toHtml();
		echo $text_m;
		echo "<br>";
		$countpropose=$this->getFirstRow('select count(*) anzahl from nodes where id<0 and '.$credentials->getId().' in (id_members, id_tech_c) '.
			'and id_members!=id_tech_c and -id not in (select id from nodes where id_members='.$credentials->getId().')');
		if ($countpropose['anzahl']>0) {
			//echo "<br>";
			echo 'Du hast <b>'.($countpropose['anzahl']>1 ? $countpropose['anzahl'].' Vorschl&auml;ge':'einen Vorschlag').'</b> für Knoteninhaber/Tech-C-Wechsel<br>';
			echo "<br>";
		}

		/*look for IPv6 address to enable right end colums*/
		$countv6=$this->getFirstRow('select count(distinct dn.id)+count(distinct dd.id) anzahl
			from   members m, nodes n
			LEFT OUTER JOIN ip6 dn on (dn.id_nodes=n.id)
			LEFT OUTER JOIN (select e.id_nodes, b.id, b.last_seen from ip6 b, devices e where b.id_devices=e.id) dd on (dd.id_nodes=n.id)
			where  m.id in (n.id_members,n.id_tech_c)
			and    n.id_members>=0 and n.id_tech_c>=0 and n.id>0
			and    m.id='.$credentials->getId());
		/*correct count of "nodes with or without devices!*/
		/*consider ipv6 devices as well? how to avoid duplicate count*/
		$stats = $this->getRows("select whom, count(distinct node_id) node_anz ,sum(case when dev_id is null and dev6n_id is null and dev6d_id is null then 1 else 0 end) as no_dev
			,count(distinct node_online) node_online
			,count(distinct dev_id) dev_anz ,count(distinct dev6n_id) dev6n_anz ,count(distinct dev6d_id) dev6d_anz
			,count(distinct dev_online) dev_online ,count(distinct dev6n_online) dev6n_online ,count(distinct dev6d_online) dev6d_online
			from ( select case when n.id_members=m.id then 'Eigene Nodes' else 'Tech-C' end as whom
			,n.id node_id, case when (n.last_seen >= now()-interval '4' hour and to_char(n.last_seen,'hh24miss')!='000000') then n.id else null end as node_online
			,d.id dev_id,  case when (d.last_seen >= now()-interval '4' hour and to_char(d.last_seen,'hh24miss')!='000000') then d.id else null end as dev_online
			,dn.id dev6n_id, case when (dn.last_seen >= now()-interval '4' hour and to_char(dn.last_seen,'hh24miss')!='000000') then dn.id else null end as dev6n_online
			,dd.id dev6d_id, case when (dd.last_seen >= now()-interval '4' hour and to_char(dd.last_seen,'hh24miss')!='000000') then dd.id else null end as dev6d_online
			from   members m, nodes n
			LEFT OUTER JOIN devices d  on (d.id_nodes=n.id)
			LEFT OUTER JOIN ip6 dn on (dn.id_nodes=n.id)
			LEFT OUTER JOIN (select e.id_nodes, b.id, b.last_seen from ip6 b, devices e where b.id_devices=e.id) dd on (dd.id_nodes=n.id)
			where  m.id in (n.id_members,n.id_tech_c)
			and    n.id_members>=0 and n.id_tech_c>=0 and n.id>0
			and    m.id=".$credentials->getId()."
			) as i group by whom order by whom"
		);
		$line_number=0;
		$totals=array('node_anz'=>0,'node_online'=>0,'no_dev'=>0,'dev_anz'=>0,'dev_online'=>0,'dev6n_anz'=>0,'dev6n_online'=>0,'dev6d_anz'=>0,'dev6d_online'=>0);
		$col_green ="green";
		$col_orange="#FFA200";
		$col_red   ="#f23a14";
		while ($line = $stats->fetchRow(DB_FETCHMODE_ASSOC)) {
			if ($line_number == 0) {
				echo 'Deine Node- und Device-&Uuml;bersicht';
				$table = new HTML_Table();
				$table->setColCount(6);
				$table->setHeaderContents(0, 0, 'Node-Gruppe');
				$table->setCellAttributes(0, 0, 'rowspan=2 style="width:104px;"');
				//$table->setColAttributes(0,'style="height:35px; width:104px;"');
				$table->setHeaderContents(0, 1, 'Nodes');
				$table->setCellAttributes(0, 1, 'colspan=3 style="width:160px;"');
				$table->setHeaderContents(0, 4, 'Devices (v4)');
				$table->setCellAttributes(0, 4, 'colspan=2 style="width:120px;"');
				if ($countv6['anzahl']>0) {
					$table->setHeaderContents(0, 6, 'Devices (v6)');
					$table->setCellAttributes(0, 6, 'colspan=2 style="width:120px;"');
				}
				$table->setHeaderContents(1, 1, 'Gesamt');
				$table->setHeaderContents(1, 2, 'mit IP');
				$table->setHeaderContents(1, 3, 'Online');
				$table->setHeaderContents(1, 4, 'Gesamt');
				$table->setHeaderContents(1, 5, 'Online');
				if ($countv6['anzahl']>0) {
					$table->setHeaderContents(1, 6, 'Gesamt');
					$table->setHeaderContents(1, 7, 'Online');
				}
			}
			$line_number++;
			$line['node_dev']=$line['node_anz']-$line['no_dev'];
			$n=round($line['node_online']/$line['node_dev']*100,0);
			$d=round($line['dev_online']/$line['dev_anz']*100,0);
			$d6=round(($line['dev6n_online']+$line['dev6d_online'])/($line['dev6n_anz']+$line['dev6d_anz'])*100,0);
			if     ($n>60) $ncol=$col_green;
			elseif ($n>30) $ncol=$col_orange;
			else           $ncol=$col_red;
			if     ($d>60) $dcol=$col_green;
			elseif ($d>30) $dcol=$col_orange;
			else           $dcol=$col_red;
			if     ($d6>60) $d6col=$col_green;
			elseif ($d6>30) $d6col=$col_orange;
			else           $d6col=$col_red;
			if ($countv6['anzahl']>0) {
				$table->addRow(array($line['whom'],
					strval($line['node_anz']),
					strval($line['node_dev']),
					"<div style='float:left'>".$line['node_online']."</div><div style='float:right; color:".$ncol.";'>".$n."%</div>",
					strval($line['dev_anz']),
					"<div style='float:left'>".$line['dev_online']."</div><div style='float:right; color:".$dcol.";'>".$d."%</div>",
					strval($line['dev6d_anz']+$line['dev6n_anz']),
					"<div style='float:left'>".strval($line['dev6d_online']+$line['dev6n_online'])."</div><div style='float:right; color:".$d6col.";'>".$d6."%</div>"
				));
			} else {
				$table->addRow(array($line['whom'],
					strval($line['node_anz']),
					strval($line['node_dev']),
					"<div style='float:left'>".$line['node_online']."</div><div style='float:right; color:".$ncol.";'>".$n."%</div>",
					strval($line['dev_anz']),
					"<div style='float:left'>".$line['dev_online']."</div><div style='float:right; color:".$dcol.";'>".$d."%</div>"
				));
			}
			foreach (array('node_anz','node_online','no_dev','node_dev','dev_anz','dev_online','dev6n_anz','dev6n_online','dev6d_anz','dev6d_online') as $key) {
				$totals[$key] += $line[$key];
			}
			$table->setCellAttributes($line_number, 1, 'style="width:40px;"');
			$table->setCellAttributes($line_number, 2, 'style="width:40px;"');
			$table->setCellAttributes($line_number, 3, 'style="width:80px;"');
			$table->setCellAttributes($line_number, 4, 'style="width:40px;"');
			$table->setCellAttributes($line_number, 5, 'style="width:80px;"');
			if ($countv6['anzahl']>0) {
				$table->setCellAttributes($line_number, 6, 'style="width:40px;"');
				$table->setCellAttributes($line_number, 7, 'style="width:80px;"');
			}
			$last_line=$line['whom'];
		}
		if ($line_number > 1) {
			$n=round($totals['node_online']/$totals['node_dev']*100,0);
			$d=round($totals['dev_online']/$totals['dev_anz']*100,0);
			$d6=round(($totals['dev6n_online']+$totals['dev6d_online'])/($totals['dev6n_anz']+$totals['dev6d_anz'])*100,0);
			if     ($n>60) $ncol=$col_green;
			elseif ($n>30) $ncol=$col_orange;
			else           $ncol=$col_red;
			if     ($d>60) $dcol=$col_green;
			elseif ($d>30) $dcol=$col_orange;
			else           $dcol=$col_red;
			if     ($d6>60) $d6col=$col_green;
			elseif ($d6>30) $d6col=$col_orange;
			else           $d6col=$col_red;
			if ($countv6['anzahl']>0) {
				$table->addRow(array('Gesamt',
					strval($totals['node_anz']),
					strval($totals['node_dev']),
					"<div style='float:left'>".$totals['node_online']."</div><div style='float:right; color:".$ncol.";'>".$n."%</div>",
					strval($totals['dev_anz']),
					"<div style='float:left'>".$totals['dev_online']."</div><div style='float:right; color:".$dcol.";'>".$d."%</div>",
					strval($totals['dev6d_anz']+$totals['dev6n_anz']),
					"<div style='float:left'>".strval($totals['dev6d_online']+$totals['dev6n_online'])."</div><div style='float:right; color:".$d6col.";'>".$d6."%</div>"
				));
			} else {
				$table->addRow(array('Gesamt',
					strval($totals['node_anz']),
					strval($totals['node_dev']),
					"<div style='float:left'>".$totals['node_online']."</div><div style='float:right; color:".$ncol.";'>".$n."%</div>",
					strval($totals['dev_anz']),
					"<div style='float:left'>".$totals['dev_online']."</div><div style='float:right; color:".$dcol.";'>".$d."%</div>"
				));
			}
		}
		if ($line_number > 0) {
			echo $table->toHtml();
		} else {
			echo 'Du hast noch keine Knoten angelegt.';
		}

		echo '<div class="tooltip"><span class="tooltiptext"></span></div>'; //this line is needed to make other tooltips work on MobileBrowsers (WTF!)
		echo <<<EOT
<script>
function checkme() {
	var a = document.getElementById('emailaddr_{$row['id']}');
	var m=a.innerHTML;
	if (isValidEmail(m)) {
		checksmtp(m, {$row['id']});
	}
}
setTimeout( function(){checkme();}, 200);
</script>
EOT;
	}
}

?>

<?php

class ConvertUtils {

	function toBoolean($input) {
		switch ($input) {
			case 'on':
			case 't':
			case 'true':
				return true;
			default:
				return false;
		}
	}

	function buildCheckbox($input, $centered = true) {
		if ($centered)
			$retVal .= '<center>';
		$retVal .= '<input type="checkbox" onClick="return false;" ';
		if (ConvertUtils::toBoolean($input))
			$retVal .= 'checked';
		$retVal .= ' />';
		if ($centered)
			$retVal .= '</center>';
		return $retVal;
	}

	function escape($input, $type) {
		$type = strtolower($type);
		switch ($type) {
			case 'bool':
			case 'boolean':
				return ConvertUtils::booleanEscape($input);
			case 'string':
			case 'str':
				return ConvertUtils::stringEscape($input);
			case 'integer':
			case 'int':
				return ConvertUtils::integerEscape($input);
			default:
				return 'UNSUPPORTED_ESCAPE_METHOD';
		}
	}

	function booleanEscape($input) {
		if (!is_bool($input))
			$input = ConvertUtils::toBoolean($input);

		if ($input)
			return 'true';
		return 'false';
	}

	function stringEscape($input) {
		if ($input == '' || $input == null)
			return 'NULL';

		$input = str_replace("\'", "'", $input);
		$input = str_replace("'", "''", $input);
		return '\''.$input.'\'';
	}

	function integerEscape($input) {
		if (is_numeric($input))
			return $input;
		if ($input=="")
			return "null";
		return 0;
	}

}

?>
function verifyForm(theForm) {
	if (document.update_nodes_form || document.insert_nodes_form) {
		if (theForm.name.value == '' || !isValidDomainLabel(theForm.name.value)) {
			return formError('Name', 'mag76 (LOWERCASE!) = Margaretengürtel 76\nMaximal 25 Zeichen', theForm.name);
		}
		if (theForm.gps_lat_deg.value != '' || theForm.gps_lat_min.value != '' || theForm.gps_lat_sec.value != '' || theForm.gps_lon_deg.value != '' || theForm.gps_lon_min.value != '' || theForm.gps_lon_sec.value) {
			if (theForm.gps_lat_deg.value < 0 || theForm.gps_lat_deg.value > 90) {
				return formError('GPS Latitude Grad', '48', theForm.gps_lat_deg);
			} else if (theForm.gps_lat_min.value < 0 || theForm.gps_lat_min.value > 59) {
				return formError('GPS Latitude Minuten', '16', theForm.gps_lat_deg);
			} else if (theForm.gps_lat_sec.value < 0 || theForm.gps_lat_sec.value > 59.999) {
				return formError('GPS Latitude Sekunden', '12.345', theForm.gps_lat_sec);
			} else if (theForm.gps_lon_deg.value < 0 || theForm.gps_lon_deg.value > 180) {
				return formError('GPS Longitude Grad', '48', theForm.gps_lon_deg);
			} else if (theForm.gps_lon_min.value < 0 || theForm.gps_lon_min.value > 59) {
				return formError('GPS Longitude Minuten', '16', theForm.gps_lon_deg);
			} else if (theForm.gps_lon_sec.value < 0 || theForm.gps_lon_sec.value > 59.999) {
				return formError('GPS Longitude Sekunden', '12.345', theForm.gps_lon_sec);
			}
		}
	} else if (document.update_devices_form || document.insert_devices_form) {
		if (theForm.name.value == '' || !isValidDomainLabel(theForm.name.value)) {
			return formError('Name', 'mag76omni (LOWERCASE!) = Margaretengürtel 76 Rundstrahler', theForm.name);
		}
		if (theForm.mac.value != '' && !isValidMacAddress(theForm.mac.value)) {
			return formError('MAC-Adresse', 'aa:bb:cc:dd:ee:ff oder aa-bb-cc-dd-ee-ff', theForm.mac);
		}
	} else if (document.update_ip6_form || document.insert_ip6_form) {
		if (theForm.name.value == '' || !isValidDomainLabel(theForm.name.value)) {
			return formError('Name', '', theForm.name);
		}
		if (!verifyIPv6(theForm.ip6.value)) {
			return formError('IPv6-Adresse', '', theForm.ip6);
		}
		if (theForm.ids.value == 'unknown') {
			return formError('Zuordnung', '', theForm.ip6);
		}
		if (theForm.mac.value != '' && !isValidMacAddress(theForm.mac.value)) {
			return formError('MAC-Adresse', 'aa:bb:cc:dd:ee:ff oder aa-bb-cc-dd-ee-ff', theForm.mac);
		}
	} else if (document.update_v642_form || document.insert_v642_form) {
		if (theForm.name.value == '' || !isValidDomainLabel(theForm.name.value)) {
			return formError('Name', '', theForm.name);
		}
		if (theForm.id_ips.value == 'unknown') {
			return formError('IPv4-Auswahl', '', theForm.name);
		}
	} else if (document.update_member_form || document.insert_member_form) {
		if ((document.update_member_form) && (document.update_member_form.transform.value=='go')) { return true; }
		if (document.insert_member_form) {
			if (!doRegexpMatch(/[a-z0-9]{1,25}/gi, theForm.nickname.value)) {
				return formError('Nickname', 'foobar\nMaximal 25 Zeichen', theForm.nickname);
			}
			if (typeof theForm.password !== 'undefined')
				 if (theForm.password.value == '' || theForm.password2.value == '' || theForm.password.value != theForm.password2.value) {
					return formError('Passwort', '', theForm.password);
				}
			if (!doRegexpMatch(/[a-zäöüß]{1,25}/gi, theForm.firstname.value)) {
				return formError('Vorname', 'Max', theForm.firstname);
			}
			if (theForm.firstname.value.length > 25) {
				return formError('Vorname', 'Max\nMaximal 25 Zeichen', theForm.firstname);
			}
			if (!doRegexpMatch(/[a-zäöüß]{1,25}/gi, theForm.lastname.value)) {
				return formError('Nachname', 'Mustermann', theForm.lastname);
			}
			if (theForm.lastname.value.length > 25) {
				return formError('Nachname', 'Mustermann\nMaximal 25 Zeichen', theForm.lastname);
			}
		} else {
			if ((typeof theForm.password !== 'undefined') && (theForm.password.value != '')) {
				if (theForm.password.value == '' || theForm.password2.value == '' || theForm.password.value != theForm.password2.value) {
					return formError('Passwort', '', theForm.password);
				}
			}
		}
		if ((typeof theForm.street !== 'undefined') && (theForm.street.value == '')) {
			return formError('Strasse', 'Favoritenstrasse', theForm.street);
		}
		if ((typeof theForm.street !== 'undefined') && (theForm.street.value.length > 100)) {
			return formError('Strasse', 'Favoritenstrasse\nMaximal 100 Zeichen', theForm.street);
		}
		if ((typeof theForm.housenumber !== 'undefined') && (!doRegexpMatch(/([0-9]{1,3}[a-z\-]{0,1}[0-9]{0,3}[0-9 \/.\-]{0,9}[a-z0-9]{0,6})/gi, theForm.housenumber.value) || theForm.housenumber.value.length > 20 )) {
			return formError('Hausnummer', '123a\noder 110-112/3/21', theForm.housenumber);
		}
		if ((typeof theForm.zip !== 'undefined') && (!doRegexpMatch(/([A-Z\-]{0,3}[0-9A-Z\ ]{4,7})/g, theForm.zip.value))) {
			return formError('PLZ', '1010\noder NL-1234 AB', theForm.zip);
		}
		if ((typeof theForm.town !== 'undefined') && (theForm.town.value == '' )) {
			return formError('Ort', 'Wien', theForm.town);
		}
		if ((typeof theForm.town !== 'undefined') && (theForm.town.value.length > 100)) {
			return formError('Ort', 'Wien\nMaximal 100 Zeichen', theForm.town);
		}
		if ((typeof theForm.telephone !== 'undefined') && (theForm.telephone.value != '' && !doRegexpMatch(/[0-9 +\/-]{1,25}/g, theForm.telephone.value))) {
			return formError('Telefon', '0123456789', theForm.telephone);
		}
		if ((typeof theForm.mobilephone !== 'undefined') && (theForm.mobilephone.value != '' && !doRegexpMatch(/[0-9 +\/-]{1,25}/g, theForm.mobilephone.value))) {
			return formError('Mobiltelefon', '+43654 1234567', theForm.mobilephone);
		}
		if ((typeof theForm.fax !== 'undefined') && (theForm.fax.value != '' && !doRegexpMatch(/[0-9 +\/-]{1,25}/g, theForm.fax.value))) {
			return formError('Fax', '0123456789', theForm.fax);
		}
		if ((typeof theForm.email !== 'undefined') && (!isValidEmail(theForm.email.value))) {
			return formError('E-Mail', 'admin@funkfeuer.at', theForm.email);
		}
		if ((typeof theForm.email !== 'undefined') && (theForm.email.value.length > 50)) {
			return formError('E-Mail', 'admin@funkfeuer.at\nMaximal 50 Zeichen', theForm.email);
		}
		if ((typeof theForm.homepage !== 'undefined') && (theForm.homepage.value != '' && !doRegexpMatch(/^(https?:\/\/).{1,}/g, theForm.homepage.value))) {
			return formError('Homepage', 'https://www.funkfeuer.at/', theForm.homepage);
		}
	} else if (document.update_voip_form || document.insert_voip_form) {
		if (theForm.voicemail.checked) {
			if (theForm.voicemail_pin.value == '' || !doRegexpMatch(/[0-9]{1,4}/g, theForm.voicemail_pin.value)) {
				return formError('Voicemail PIN', '1234', theForm.voicemail_pin);
			}
			if (theForm.voicemail_delay.value == '' || theForm.voicemail_delay.value < 5) {
				return formError('Voicemail Delay', '>= 5', theForm.voicemail_delay);
			}
		}
		if (theForm.h323.checked) {
			if (theForm.h323_ip.value == '') {
				return formError('H323 IP', '123.123.123.123', theForm.h323_ip);
			}
		} else {
			if (theForm.h323_ip.value != '') {
				return formError('H323 IP', 'Leer lassen!', theForm.h323_ip);
			}
		}
	} else if (document.update_voip_sip_form || document.insert_voip_sip_form) {
		if (theForm.secret.value == '' || !doRegexpMatch(/[A-Za-z0-9]{4,16}/g, theForm.secret.value)) {
			return formError('Secret', 'abc1234 (4-16 Stellen)', theForm.secret);
		}
		if (theForm.port.value != '' && !doRegexpMatch(/[0-9]{1,6}/g, theForm.port.value)) {
			return formError('Port', '1234', theForm.port);
		}
		if (theForm.provider.checked) {
			return formError('Provider', 'Wird dzt. nicht supported', theForm.provider);;
			/*
			if (theForm.provider_host.value == '' || theForm.provider_fromuser.value == '' || theForm.provider_fromdomain.value == '' || theForm.provider_secret.value == '') {
				return formError('Provider', 'Alle Provider bezogenen Felder sind verpflichtend', theForm.provider);
			}
			*/
		}
	} else if (document.update_meetme_form || document.insert_meetme_form) {
		if (theForm.meetme_pin.value != '') {
			if (theForm.meetme_pin.value != '' && !doRegexpMatch(/[0-9]{1,4}/g, theForm.meetme_pin.value)) {
				return formError('PIN', '1234', theForm.meetme_pin);
			}
			if (theForm.meetme_admin_pin.value != '' && !doRegexpMatch(/[0-9]{1,4}/g, theForm.meetme_admin_pin.value)) {
				return formError('Admin PIN', '1234', theForm.meetme_admin_pin);
			}
		}
	}
	theForm.submit.disabled=true;
	theForm.submit.value = "Please wait...";
	return true;
}

function formError(errorField, errorExample, errorElement) {
	if (errorExample != '') {
		alert("Eingabefehler bei "+errorField+"\n"+'Beispiel: '+errorExample);
	} else {
		alert("Eingabefehler bei "+errorField);
	}
	errorElement.focus();
	errorElement.select();
	return false;
}

function doRegexpMatch(theRegexp, theValue) {
	if (theValue.match(theRegexp) == theValue) {
		return true;
	} else {
		return false;
	}
}

function isValidEmail(str) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(str);
}

function isValidDomainLabel(str) {
	return doRegexpMatch(/[a-z0-9]([a-z0-9\-]{0,23}[a-z0-9])?/ig, str);
}

function isValidMacAddress(str) {
	return doRegexpMatch(/^([0-9a-f]{2}[:-]?[0-9a-f]{2}[:-]?[0-9a-f]{2}[:-]?[0-9a-f]{2}[:-]?[0-9a-f]{2}[:-]?[0-9a-f]{2})$/gi, str);
}

function askDelete() {
	return confirm('Wirklich löschen?');
}

function askResign() {
	return confirm('Wirklich Tech-Contact Zuordnung löschen?');
}

function askDecline() {
	return confirm('Wirklich Transferantrag verwerfen?');
}

function ip6_device_to_name(str) {
	if (document.update_ip6_form) { var theForm=document.update_ip6_form; }
	else if (document.insert_ip6_form) { var theForm=document.insert_ip6_form; }
	else { return; }
	var type=str.split(' ');;
	if (type[0]=='Device') {
		var name=type[1].split('.');
		theForm.name.value=name[0];
		theForm.name.readOnly=true;
	} else {
		theForm.name.readOnly=false;
	}
}

function propose(i) {
	if (document.update_ip6_form) { var theForm=document.update_ip6_form; }
	else if (document.insert_ip6_form) { var theForm=document.insert_ip6_form; }
	else { return; }
	if (i==1) {
		if (isValidMacAddress(theForm.mac.value)) {
			theForm.ip6.value=joinUpIp6(theForm.mac.value);
		} else {
			theForm.ip6.value='2a02:61:0:ff:AABB:CCff:feDD:EEFF';
		}
	} else if (i==2) {
		var n=parseInt(theForm.id_nodes.value,10);
		theForm.ip6.value='2a02:61:'+n.toString(16)+'::1';
	} else if (i==3) {
		if (isValidMacAddress(theForm.mac.value)) {
			theForm.ip6.value=joinUpIp6(theForm.mac.value);
		} else {
			alert('fill in propper MAC address first');
		}
	} else {
		return;
	}
}

function verifyIPv6(str) {
	var expression = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;
	if (expression.test(str)) {
		if (str=='2a02:61:0:ff:AABB:CCff:feDD:EEFF') return false;
		return true;
	} else {
		return false;
	}
}

function gps_to_decimal() {
	if (document.update_nodes_form) { var theForm=document.update_nodes_form; }
	else if (document.insert_nodes_form) { var theForm=document.insert_nodes_form; }
	else { return; }
	theForm.gps_lat_deg.value=theForm.gps_lat_deg.value.replace(',','.');
	theForm.gps_lat_min.value=theForm.gps_lat_min.value.replace(',','.');
	theForm.gps_lat_sec.value=theForm.gps_lat_sec.value.replace(',','.');
	if ( isNaN(parseFloat(theForm.gps_lat_deg.value)) || isNaN(parseFloat(theForm.gps_lat_min.value)) || isNaN(parseFloat(theForm.gps_lat_sec.value)) ) {
		theForm.lat.value="";
	} else {
		theForm.lat.value=Math.round((parseFloat(theForm.gps_lat_deg.value) +parseFloat(theForm.gps_lat_min.value)/60 +parseFloat(theForm.gps_lat_sec.value)/3600)*10000000) /10000000;
	}
	theForm.gps_lon_deg.value=theForm.gps_lon_deg.value.replace(',','.');
	theForm.gps_lon_min.value=theForm.gps_lon_min.value.replace(',','.');
	theForm.gps_lon_sec.value=theForm.gps_lon_sec.value.replace(',','.');
	if ( isNaN(parseFloat(theForm.gps_lon_deg.value)) || isNaN(parseFloat(theForm.gps_lon_min.value)) || isNaN(parseFloat(theForm.gps_lon_sec.value)) ) {
		theForm.lon.value="";
	} else {
		theForm.lon.value=Math.round((parseFloat(theForm.gps_lon_deg.value) +parseFloat(theForm.gps_lon_min.value)/60 +parseFloat(theForm.gps_lon_sec.value)/3600)*10000000) /10000000;
	}
	return;
}
function gps_to_dms() {
	if (document.update_nodes_form) { var theForm=document.update_nodes_form; }
	else if (document.insert_nodes_form) { var theForm=document.insert_nodes_form; }
	else { return; }
	theForm.lat.value=theForm.lat.value.replace(',','.');
	if (!isNaN(parseFloat(theForm.lat.value))) {
		theForm.gps_lat_deg.value = Math.floor(theForm.lat.value);
		theForm.gps_lat_min.value = Math.floor(theForm.lat.value*60) % 60;
		var s1 = theForm.lat.value*3600 % 60;
		theForm.gps_lat_sec.value = Math.round(1000 * s1)/1000;
	}
	theForm.lon.value=theForm.lon.value.replace(',','.');
	if (!isNaN(parseFloat(theForm.lon.value))) {
		theForm.gps_lon_deg.value = Math.floor(theForm.lon.value);
		theForm.gps_lon_min.value = Math.floor(theForm.lon.value*60) % 60;
		var s2 = theForm.lon.value*3600 % 60;
		theForm.gps_lon_sec.value = Math.round(1000 * s2)/1000;
	}
	return;
}

var waitingCheck;
var lastVerified='x';
var needCheck=true;
var lastResult='?';

function updateCheckSymbol(str,id) {
	var c=document.getElementById('resultcode_'+id);
	c.innerHTML=str;
}

function checkValidEmail(str, id) {
	if (str == lastVerified) {
		return isValidEmail(str);
	} else {
		needCheck = true;
		clearTimeout(waitingCheck);
		updateCheckSymbol('<i class="far fa-question-circle fa-lg" style="color:gray;"></i><span class="tooltiptext">Noch nicht überprüft</span>', id);
		var res=isValidEmail(str);
		if (res) {
			 waitingCheck = setTimeout(function() { checksmtp(str, id); }, 3000);
		}
		return res;
	}
}

function checksmtp(str, id) {
	if ((needCheck) && (isValidEmail(str)) && (str !== lastVerified)) {
		var icon='fa fa-spinner fa-spin'
		var color='blue';
		updateCheckSymbol('<i class="'+icon+' fa-lg" style="color:'+color+';"></i><span class="tooltiptext">Prüfung läuft...</span>', id);
		var Httpreq = new XMLHttpRequest();
		Httpreq.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				setTimeout( function(){
					var json=Httpreq.responseText;
					var b = JSON.parse(json);
					switch(b.code) {
						case '250':  //ok
							b.verbose='erfolgreich verifiziert';
							icon='far fa-check-circle'; color='green'; break;
						case '450':  //greylisting? most probably ok
						case '451':
						case '452':
						case '498':  // in timeout after greylisting notification
							icon='fas fa-exclamation-circle'; color='green'; break;
						case '503':  //invalid command at this point
						case '530':  //requested STARTLS
							icon='fas fa-exclamation-circle'; color='orange'; break;
						case '421':  //sender address greylisted, receipient not checked
							b.verbose='nicht gepr&uuml;ft, Absender greylisted';
							icon='far fa-frown'; color='orange'; break;
						case '454':  //relay access denied
						case '499':  //dummy-code: user-unknown from verbose-text
						case '550':  //user mailbox unknown
						case '552':  //mailbox quota exeeded - mailbox not used anymore?
						case '554':
							icon='far fa-times-circle'; color='red'; break;
						case '002':  //wrong email format, no smtp check performed
						case '994':  //no MX record
						case '995':  //incorrect mail address
						case '996':  //could not establish a connection to mailserver
						case '997':
						case '998':  //all connections failed
							icon='fas fa-exclamation-triangle'; color='red'; break;
						case '999':  //did not recognize smtp result
							icon='far fa-question-circle'; color='orange'; break;
						case '001':  //pending check in another script execution
							icon='far fa-question-circle fa-spin'; color='blue';
							needCheck=true; lastVerified='y';
							setTimeout( function(){checksmtp(str, id);}, 2000);
							break;
						default:
							icon='far fa-question-circle'; color='black'; break;
					}
					if (b.code=='250' && b.ca=='250') {
						b.verbose='verifiziert, CatchAll erkannt';
						icon='fas fa-exclamation-triangle';
					}
					lastResult='<i class="'+icon+' fa-lg" style="color:'+color+';"></i><span class="tooltiptext">'+b.verbose+'</span>';
					updateCheckSymbol(lastResult, id);
				}, 10);
			}
		};
		Httpreq.ontimeout = function() {
			icon='far fa-question-circle'; color='blue';
			lastResult='<i class="'+icon+' fa-lg" style="color:'+color+';"></i><span class="tooltiptext">Zeitüberschreitung</span>';
			updateCheckSymbol(lastResult, id);
		};
		Httpreq.onerror = function() {
			icon='far fa-question-circle'; color='blue';
			lastResult='<i class="'+icon+' fa-lg" style="color:'+color+';"></i><span class="tooltiptext">Verbindungsfehler</span>';
			updateCheckSymbol(lastResult, id);
		};
		Httpreq.timeout = 90000; //90 seconds
		Httpreq.open("POST",'verifymail.php?id='+id,true);
		Httpreq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		Httpreq.send(encodeURI('m='+encodeURIComponent(str)+'&i='+id));
		needCheck = false;
		lastVerified=str;
	}
}

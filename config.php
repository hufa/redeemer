<?php

if (file_exists('config_local.php'))
	include_once('config_local.php');

@define('BASE',     'frontend');
@define('TITLEBAR', 'Redeemer Wien');

@define('DB_HOST', 'localhost');
@define('DB_USER', 'redeemer');
@define('DB_PASS', 'xxxxxxxxxxxxxx');
@define('DB',      'redeemer');

@define('SMTP_DAYS', 30);

define('ADMIN_ROLE_NAME', 'Admin');
define('SUPERUSER_ROLE_NAME', 'Superuser');

define('SMARTY_TEMPLATE_DIR', 'templates');
define('SMARTY_COMPILE_DIR',  'tmp/compile');
define('SMARTY_CACHE_DIR',    'tmp/cache');
define('SMARTY_CONFIG_DIR',   'tmp/config');

@define('MEMBER_SUBSCRIBE_MAIL', true);
@define('MEMBER_SUBSCRIBE_MAIL_FROM', 'admin@funkfeuer.at');
@define('MEMBER_SUBSCRIBE_MAIL_SUBJECT', 'Frontend Zugangsdaten');

@define('DEBUG', false);

define('COLWIDTH_NAME','100');
define('COLWIDTH_TECHC','130');
define('COLWIDTH_MAC','125');
define('COLWIDTH_MAP','30');
define('COLWIDTH_AAAA','50');
define('COLWIDTH_DEVICES','50');
define('COLWIDTH_GPS','90');
define('COLWIDTH_LASTSEEN','120');
define('COLWIDTH_IPV6','220');

@define('MAX_LIMITED', 30);
@define('MAX_NOLIMIT', 150);
?>

<?php

function storeCredentials() {
	global $credentials;

	$_SESSION['credentials'] = serialize($credentials);
}

function restoreCredentials() {
	global $credentials;

	if (!$_SESSION['credentials'])
		$credentials = new Credentials();
	else
		$credentials = unserialize($_SESSION['credentials']);
}

function checkRights($search, $where) {
	global $databaseUtils;
	global $credentials;

	switch($where) {
		case 'members':
			$search_array = $databaseUtils->my_stuff['members'];
			break;
		case 'nodes':
			$search_array = $databaseUtils->my_stuff['nodes'];
			break;
		case 'owner':
			$search_array = $databaseUtils->my_stuff['owner'];
			break;
		case 'devices':
			$search_array = $databaseUtils->my_stuff['devices'];
			break;
		case 'v642':
			$search_array = $databaseUtils->my_stuff['devices'];
			break;
		case 'ip6':
			$search_array = $databaseUtils->my_stuff['ip6'];
			break;
		case 'voips':
			$search_array = $databaseUtils->my_stuff['voips'];
			break;
		case 'voip_sips':
			$search_array = $databaseUtils->my_stuff['voip_sips'];
			break;
		case 'meetmes':
			$search_array = $databaseUtils->my_stuff['meetmes'];
			break;
	}

	if (is_numeric(array_search($search, $search_array)) || empty($search)) {
		return true;
	} else {
		if (($credentials->isSuperuser()) && ($where=='members')) return false;
		assignError('Unzureichende Rechte',$where.' '.$search);
		return false;
	}
}

function assignError($error, $detail = null) {
	global $smarty;

	$smarty->assign('error', $error);
	$smarty->assign('error_detail', $detail);
}

function debugMessage($msg) {
	global $smarty;

	if (DEBUG)
		$smarty->assign('debug_message', $msg);
}

function logoff() {
	session_destroy();
	session_start();
	header('Location: '.$_SERVER['PHP_SELF']);
	exit;
}

function memberSubscribeMail($email, $nickname, $password) {
	if (MEMBER_SUBSCRIBE_MAIL) {
		$smarty = new Smarty;
		$smarty->template_dir = SMARTY_TEMPLATE_DIR;
		$smarty->compile_dir = SMARTY_COMPILE_DIR;
		$smarty->cache_dir = SMARTY_CACHE_DIR;
		$smarty->config_dir = SMARTY_CONFIG_DIR;
		$smarty->assign('nickname', $nickname);
		$smarty->assign('password', $password);
		$mail_content = $smarty->fetch('member_subscribe_mail.tpl');

		$mail_headers  = "From: ".MEMBER_SUBSCRIBE_MAIL_FROM."\r\n";
		$mail_headers  = "To: ".$email."\r\n";
		$mail_headers .= "MIME-Version: 1.0\r\n";
		$mail_headers .= "Content-Type: text/plain; charset=utf-8\r\n";
		$mail_headers .= "Content-Transfer-Encoding: quoted-printable\r\n";
		mail($email, MEMBER_SUBSCRIBE_MAIL_SUBJECT, $mail_content, $mail_headers, "-f".MEMBER_SUBSCRIBE_MAIL_FROM);
	}
}

function format_date($str) {
	if ($str=='') return 'unknown';
	if (date('H:i',strtotime($str)) == '00:00') return date('Y-m-d',strtotime($str));
	return str_replace(' ','&nbsp;', date('Y-m-d H:i',strtotime($str)));
}
function format_dateonly($str) {
	if ($str=='') return 'unknown';
	return date('Y-m-d',strtotime($str));
}
function expand_ipv6($ip) {
	$hex = unpack("H*hex", inet_pton($ip));
	$ip = substr(preg_replace("/([A-f0-9]{4})/", "$1:", $hex['hex']), 0, -1);
	return $ip;
}

function shorten_ipv6($ip) {
	return inet_ntop(inet_pton($ip));
}

function checkIp6Range($ip6, $mode = 0, $nodeid = 0) {
	// Modes: 0-all, 1-EUI64, 2-v642src, 3-v642dest, 4-userblock
	$ip6_check=strtolower(shorten_ipv6($ip6));
	if       (eregi("^2a02:61:0:ff:.*$",$ip6_check)) {
		if (in_array($mode,array(0,1))) { return true; } else {
			assignError('IPv6-Adresse Format EUI64/Node 0 hier nicht erlaubt');
			return false;
		}
	} elseif (eregi("^2a02:61:0:ee:0:.*$",$ip6_check)) {
		if (in_array($mode,array(0,2))) { return true; } else {
			assignError('IPv6-Adresse von v642-Source hier nicht erlaubt');
			return false;
		}
	} elseif (eregi("^2a02:61:0:ee:1:.*$",$ip6_check)) {
		if (in_array($mode,array(0,3))) { return true; } else {
			assignError('IPv6-Adresse von v642-Destination hier nicht erlaubt');
			return false;
		}
	} elseif (eregi("^2a02:61:".dechex($nodeid).":.*$",$ip6_check)) {
		if ($nodeid <= 0) {
			assignError('IPv6-Adresse aus Userblock von Node-ID '.$nodeid.' hier nicht erlaubt');
			return false;
		}
		if (in_array($mode,array(0,4))) { return true; } else {
			assignError('IPv6-Adresse aus Userblock hier nicht erlaubt');
			return false;
		}
	} else {
		assignError('IPv6-Adresse ('.$ip6_check.' | '.$nodeid.') nicht erlaubt');
		return false;
	}
	assignError('Falsche IPv6-Range ('.$ip6_check.')');
	return false;
}

?>

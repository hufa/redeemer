<?php
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);

// die on unsave charachters in request-URI to avoid XSS
if (preg_match("/[\"\'<>#%{}|\^~\[\]`]/", $_SERVER['QUERY_STRING'])) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request ', true, 400);
  exit;
}

ini_set('include_path', '.:lib:classes');
ini_set('session.cookie_samesite', 'Strict');
ini_set("session.cookie_secure", 1);

include_once('HTML/Form.php');
include_once('HTML/Table.php');
include_once('DB.php');
include_once('Smarty/Smarty.class.php');

include_once('config.php');
include_once('functions.php');

include_once('DatabaseUtils.php');
include_once('Credentials.php');
include_once('ConvertUtils.php');
include_once('Misc.php');
include_once('Forms.php');
include_once('Tables.php');

session_start();
if (preg_match('/\/'.BASE.'\//', $_SESSION['uri']) == 0) {
	session_destroy();
	session_start();
}
$_SESSION['uri'] = $_SERVER['REQUEST_URI'];

if ((($_GET['section']=='nodes') || ($_GET['section']=='devices') || ($_GET['section']=='ip6') || ($_GET['section']=='v642'))
		&& (($_GET['action']=='insert') || ($_GET['action']=='update'))
		&& ($_POST['name']!='')) {
	if (preg_replace ( '/[a-z0-9]([a-z0-9\-]{0,23}[a-z0-9])?/i' , 'correct' , $_POST['name']) != 'correct' )
		die('<h1>ERROR: invalid node/device name specified ('.$_POST['name'].')</h1>allowed characters a-z 0-9 and -<br>maximum length: 25');
}

restoreCredentials();

if ($_GET['logoff'])
	logoff();

$databaseUtils = new DatabaseUtils();
$forms = new Forms();
$tables = new Tables();

$smarty = new Smarty;
$smarty->template_dir = SMARTY_TEMPLATE_DIR;
$smarty->compile_dir = SMARTY_COMPILE_DIR;
$smarty->cache_dir = SMARTY_CACHE_DIR;
$smarty->config_dir = SMARTY_CONFIG_DIR;

if (!$credentials->isLoggedIn() && !empty($_POST['nickname']) && !empty($_POST['password'])) {
	if (isset($_POST["CSRFtoken"], $_COOKIE["CSRFtoken"])) {
		if ($_POST["CSRFtoken"] == $_COOKIE["CSRFtoken"]) {
			$credentials->load($_POST['nickname'], $_POST['password']);
		} else {
			header($_SERVER['SERVER_PROTOCOL'] . ' 401 Unauthorized ', true, 401);
			print 'Login method error - your login token might have been expired. Please go back, reload the page and try again';
			exit; // token not correct
		}
	} else {
		header($_SERVER['SERVER_PROTOCOL'] . ' 406 Not Acceptable ', true, 406);
		print 'Login method error - your login token might have been expired. Please go back, reload the page and try again';
		exit; // token missing
	}
}

if ( (($credentials->isAdmin()) || ($credentials->isSuperuser())) &&
	 (($_POST['ingroups']) || ($_POST['offgroups']) || ($_POST['search']))
	) { /*save search and perform an external redirect*/
	$_SESSION['ingroups']=$_POST['ingroups'];
	$_SESSION['offgroups']=$_POST['offgroups'];
	$_SESSION['search']=$_POST['search'];
	if (isset($_POST['limitresult'])) {$_SESSION['limitresult']='checked="checked"';} else {unset($_SESSION['limitresult']);} 
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: ".$_SERVER['REQUEST_URI']);
	exit;
}
if (!isset($_SESSION['search'])) $_SESSION['limitresult']='checked="checked"';

if ($credentials->getId())
	$databaseUtils->fetchMyStuff();

$smarty->assign('html_title', TITLEBAR.(isset($_GET['section'])?' - '.ucfirst($_GET['section']):' - Main'));
$smarty->assign('PHP_SELF', $_SERVER['PHP_SELF']);
$smarty->assign('REQUEST_URI', $_SERVER['REQUEST_URI']);
$smarty->assign('section', $_GET['section']);
if (($_GET['section'] == 'devices') || ($_GET['section'] == 'ip6') || ($_GET['section'] == 'v642')) {
	$tmp=$databaseUtils->getFirstRow('SELECT name from nodes where id='.ConvertUtils::escape($_GET['id_nodes'], 'integer'));
	$node_name=$tmp['name'];
	$smarty->assign('node_name', $node_name);
}
$type_of_ip=type_of_ip($_SERVER['REMOTE_ADDR']);
if ($type_of_ip) $smarty->assign('add_on',' | <span style="color:'.($type_of_ip=='IPv6' ? '#91b810; font-weight: bold' : '#286090').';">'.$type_of_ip.'</span>');
$smarty->assign('action', $_GET['action']);
$smarty->assign('do', $_POST['do']);
$smarty->assign('id', $_GET['id']);
$smarty->assign('id_members', $_GET['id_members']);
$smarty->assign('id_nodes', $_GET['id_nodes']);
$smarty->assign('id_devices', $_GET['id_devices']);
$smarty->assign('id_voip_extensions', $_GET['id_voip_extensions']);
$smarty->assign('id_voip_sip', $_GET['id_voip_sip']);
$smarty->assign('ingroups',  $_SESSION['ingroups']);
$smarty->assign('offgroups', $_SESSION['offgroups']);
$smarty->assign('search',    $_SESSION['search']);
$smarty->assign('limitresult',$_SESSION['limitresult']);
$smarty->assign('original_id',  $_SESSION['original_id']);
$smarty->assign('nickname', $credentials->getNickname());
if ($credentials->isAdmin())     $smarty->assign('adminlevel', 'ADMIN');
if ($credentials->isSuperuser()) $smarty->assign('adminlevel', 'Admin');
$smarty->assign('credentials', $credentials);
$smarty->assign('databaseUtils', $databaseUtils);
$smarty->assign('forms', $forms);
$smarty->assign('tables', $tables);

$smarty->display('index.tpl');

storeCredentials();

?>
